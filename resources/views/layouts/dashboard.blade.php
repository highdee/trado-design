<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Dashboard | Trado Global </title>
    {{-- ICONS --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
     <!-- GOOGLE FONTS -->
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&family=Roboto:wght@400;500;700;900&display=swap"
        rel="stylesheet">
    <!-- BS4 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <!-- Styles -->
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main_style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/farmshop.css') }}" rel="stylesheet">
    <link href="{{ asset('css/settings.css') }}" rel="stylesheet">
    <link href="{{ asset('css/savings.css') }}" rel="stylesheet">
    @yield('header-link')
</head>
<body>
    <div id="dashboard">
        <div class="navs">
            <!-- MOBILE SCREEN NAV -->
            <nav id="nav-sm">
                <div class="trd-1635-dss">
                    <ul class="trd-1635-nav">
                        <li class="trd-1635-items">
                            <a href="/user/dashboard" class="trd-1635-links active">
                                <img src="{{ asset('/images/index.svg') }}">
                                <h6>Dashboard</h6>
                            </a>
                        </li>
                        <li class="trd-1635-items">
                            <a href="/user/dashboard/farmshop" class="trd-1635-links">
                                <img src="{{ asset('/images/street_shop.svg') }}">
                                <h6>Farmshop</h6>
                            </a>
                        </li>
                        <li class="trd-1635-items">
                            <a href="/user/dashboard/savings" class="trd-1635-links">
                                <img src="{{ asset('/images/savings.svg') }} " class="filter-gry">
                                <h6>Savings</h6>
                            </a>
                        </li>
                        <li class="trd-1635-items">
                            <a href="/user/dashboard/transaction" class="trd-1635-links">
                                <img src="{{ asset('/images/24-payment.svg') }}">
                                <h6>Transaction</h6>
                            </a>
                        </li>
                        <li class="trd-1635-items">
                            <a href="/user/dashboard/settings" class="trd-1635-links">
                                <img src="{{ asset('/images/settings.svg') }}">
                                <h6>Settings</h6>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- XL  SCREEN NAV -->
            <nav id="nav-lg">
                <div class="trd-nav-lg">
                    <div class="trd-nav-js43-top">
                        <a href="/" class="top-nav-img">
                            <img src="{{ asset('/images/trado-logo.png') }}" alt="_trado-global">
                        </a>
                    </div>
                    <div class="trd-nav-js45-top">
                        <ul class="trd-navbar">
                            <li class="trd-nav-item">
                                <a href="/user/dashboard" class="trd-nav-link {{ Request::is('user/dashboard') ? 'active':'' }}">
                                    <img src="{{ asset('/images/index.svg') }}">
                                    <span>Dashboard</span>
                                </a>
                            </li>
                            <li class="trd-nav-item">
                                <a href="/user/dashboard/farmshop" class="trd-nav-link {{ Request::is('user/dashboard/farmshop') ? 'active':'' }}">
                                    <img src="{{ asset('/images/street_shop.svg') }}">
                                    <span>Farmshop</span>
                                </a>
                            </li>
                            <li class="trd-nav-item">
                                <a href="/user/dashboard/savings" class="trd-nav-link {{ Request::is('user/dashboard/savings') ? 'active':'' }}">
                                    <img src="{{ asset('/images/savings.svg') }} " class="filter-gry">
                                    <span>Savings</span>
                                </a>
                            </li>
                            <li class="trd-nav-item">
                                <a href="/user/dashboard/transaction" class="trd-nav-link {{ Request::is('user/dashboard/transaction') ? 'active':'' }}">
                                    <img src="{{ asset('/images/24-payment.svg') }}">
                                    <span>Transaction</span>
                                </a>
                            </li>
                            <li class="trd-nav-item">
                                <a href="/user/dashboard/settings" class="trd-nav-link {{ Request::is('user/dashboard/settings') ? 'active':'' }}">
                                    <img src="{{ asset('/images/settings.svg') }}">
                                    <span>Settings</span>
                                </a>
                            </li>
                        </ul>
                        <div class="trd-nav-js45-bottom">
                            <div>
                                <a href="#" class="trd-nav-link active">
                                    <img src="{{ asset('/images/XMLID_2.png') }}">
                                    <span>Log Out</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <!-- PAGE CONTENT -->
        @yield('content')
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script>
    </script>
    <script>
        function removeDom(domObjec, cls, modal_true = true){
            if(modal_true){
                $(domObjec).css('transform', 'translateY(0)')
            }
            setTimeout(function() {
                $(domObjec).removeClass(cls);
                $('#pagemodal').removeClass('open')
            }, 350);
            $(domObjec).attr('data-active', 'false');
        }

        function addDom(domObjec, cls, modal_true = true){
            $('#pagemodal').addClass('open')
            $(domObjec).addClass(cls);
            setTimeout(function() {
                if(modal_true){
                    $(domObjec).css('transform', 'translateY(25px)')
                }
            }, 100);
            $(domObjec).attr('data-active', 'true');
        }

        (function getModal(){
            document.querySelector('#pagemodal').addEventListener('click', function(event){
                if(event.target.getAttribute('data-name') == 'page-modal'){
                    let childs = this.children;

                    for(let i = 0; i < childs.length; i++){
                        if(childs[i].getAttribute('data-active') == 'true'){
                            childs[i].setAttribute('data-active', 'false');
                            childs[i].classList.remove('show');
                            // childs[i].style.transform = "translateY(0)";
                            break;                       
                        }
                    }
                    
                    this.classList.remove('open');
                    return null
                }
            });
        })();
    </script>
    @yield('script')
</body>
</html>