<!-- Navbar -->
<div class="container-fluid navbar-wrapper p-0 width">
    <div class="row  ">
        <div class="col-12 p-0 nav-col">
            <nav class="navbar navbar-expand-lg  navbar-light bg-light">
                <a class="navbar-brand" href="/">
                    <img src="images/TRADO COLOR.png" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="tog"></div>
                    <div class="tog"></div>
                    <div class="tog"></div>
                    <span class="fa fa-remove"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/about-us">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/blog">Blog</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/faqs">FAQ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contact-us">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a href="/login" class="nav-link nav-login">Login in</a>
                        </li>
                        <li class="nav-item">
                            <a href="/sign-up" class="nav-link nav-signup">Create an Account</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
