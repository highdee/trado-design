@extends('layouts.app')

@section('content')
   <div class="container-fluid p-0">
       <div class=" contact-wrapper">
           <div class="row m-0 contact-width">
               <div class="col-12 col-sm-12 col-md-5 p-0">
                   <div class="contact-head">
                       <h2 class="contact-h2">HELLO,</h2>
                       <h2 class="contact-h2">Got a Question?</h2>
                       <p class="contact-p">
                           Whether you have questions about investments, features, or anything
                           else, fill out the form, and we’ll be in touch as soon as possible.
                       </p>
                   </div>
               </div>
           </div>

       </div>
       <div class="row contact-width" id="contact-wrapper">
           <div class="col-sm-12 col-md-5">
               <img src="{{asset('images/Contact Us trado.svg')}}" class="contact-image" alt="">
           </div>
           <div class="col-sm-12 col-md-7">
               <div class="contactForm-wrapper">
                   <form action="">
                       <div class="form-group">
                           <label for="name">First Name</label>
                           <input type="text" class="form-control" placeholder="Your main name">
                       </div>
                       <div class="form-group">
                           <label for="email">E-mail Address</label>
                           <input type="emailt" class="form-control" placeholder="e.g tradofamily@gmail.con">
                       </div>
                       <div class="form-group">
                           <label for="name">Message</label>
                           <textarea name="" id="" placeholder="HOW CAN WE HELP YOU TODAY?" ></textarea>
                       </div>
                        <button class="btn btn-success btn-block btn-lg contact-btn">SUBMIT</button>
                   </form>
               </div>
           </div>
       </div>
       <div class="contact-map-container">
           <div class="contact-map">
           <div class="contact-overlay"></div>
           <div class="map-info-wrapper contact-width">
                <div class="map-text-wrapper">
                    <div class="map-icon-wrap">
                        <img src="{{asset('images/location_1.png')}}" alt="" class="map-icon">
                    </div>
                    <div class="m-txt-wrapper">
                        <h4 class="map-location">
                            No. 10 Obafemi Awolowo Way, Treasure plaza Igbona, Oshogbo, Osun State
                        </h4>
                        <div class="map-phone-icon">
                            <div class="m-phone-wrapper">
                                <img src="{{asset('images/phone-1.png')}}" alt="" class="map-phone">
                            </div>
                            <p class="map-phone-numb">+2349034484541</p>
                        </div>
                        <p class="click-me-p">
                            To send Us a message through mail,
                            <a href="#" class="click-me-link">
                                Click here
                                <span class="click-me-line"></span>
                            </a>
                        </p>
                    </div>
                </div>
           </div>
       </div>
       </div>
   </div>
@endsection
