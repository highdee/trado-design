<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- BS4 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&family=Roboto:wght@400;500;700;900&display=swap"
        rel="stylesheet">
    <!-- Style Sheet -->
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/faqs.css')}}">
</head>

<body>
    @include('pages.navbar')
    <main>
        <div class="wrapper">
            <div class="banner-bg"></div>
            <div class="container trd-9924-ps px-0">
                <div class="row">
                    <div class="trd-9924-wrapper">              
                       <div class="trd-9924">
                            <div class="trd-9924-top">
                                <div class="title">
                                    <h5>Frequently Asked Questions</h5>
                                    <p>Have a Questions? We have Answers!</p>
                                </div>
                                <div class="trd-9924-form">
                                    <form novalidate>
                                        <span class="search-icon">
                                            <img src="{{asset('images/search-icon.svg')}}">
                                        </span>
                                        <div class="input-group">
                                            <input type="text" class="input-control" placeholder="Type something here and press enter to search">
                                        </div>
                                        <button type="submit" class="btn search-btn">
                                            <span>Search</span>
                                            <img src="{{asset('images/arrow_simple.svg')}}">
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <div class="trad-faqs">
                                <div class="faqs-accordion">
                                    <details>
                                        <summary>What is Trado Global</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>Why choose Trado Global</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>What do I gain with Trado Global</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>How is my Money spent</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>What is the minimum I can invest with?</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>How do I pay for a farm plantation?</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>Is there any Insurance cover for my investments?</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>How do I know my money is safe?</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>Can I visit the farm?</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>When do I get my returns on investments?</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>Do you have a money-bank policy on investments?</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>When is the next investments cycle</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                    <details>
                                        <summary>What is Trado e-wallet</summary>
                                        <p>
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni.
                                            Minima, illum odio quo labore corrupti autem esse perspiciatis quos rerum voluptatibus magni, 
                                            aspernatur consectetur nisi sapiente nemo aliquid incidunt. Quasi, assumenda.
                                        </p>
                                    </details>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>
            </div>
       </div>
    </main>
    @include('pages.footer')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

</html>