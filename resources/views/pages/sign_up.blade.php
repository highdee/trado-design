<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- BS4 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&family=Roboto:wght@400;500;700;900&display=swap"
        rel="stylesheet">
    <!-- Style Sheet -->
    <link rel="stylesheet" href="{{asset('css/user_utilities.css')}}">
</head>

<body>
    <main>
       <div class="wrapper">
            <div class="image-intro-container h-100">
                <div class="trd-js32 trd-js442">
                   <div class="abs abs-trd-xl">
                        <div class="top">
                            <div class="top-brand">
                                <img src="{{ asset('images/trado-logo.png') }}" alt="_logo">
                            </div>
                            <div class="header">
                                <h5>Create your free <br> Trado Account</h2>
                                <p>One account for everything money, <br> including investments</p>
                            </div>
                        </div>
                        <div class="trd-js434">
                            <img src="{{ asset('images/register2.png') }}" alt="" class="trd-width-lg">
                        </div>
                   </div>
                </div>
            </div>
            <!-- SIGN UP CONTAINER -->
            <div class="container h-max-content h-100">
                <div class="row h-100">
                    <div class="col-12 trd-1112 h-100">
                        <div class="trd-1112-bg"></div>
                        <div class="trd-1112-wrapper">              
                            <div class="trd-1112-top trd-xvz">
                                <div class="top-img">
                                    <img src="{{asset('images/trado_white.png')}}" alt="_logo">
                                </div>
                            </div>
                            <div class="trd-1112-form trd-xvz">
                                <div class="header">
                                    <h5>Create your free Trado Account</h2>
                                    <p>One account for everything money, including investments</p>
                                </div>
                                <div class="sign_up_form">
                                    <form novalidate>
                                        <div class="container px-0">
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>First Name</label>
                                                        <input type="text" class="form-control" placeholder="Your main name">
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Last Name</label>
                                                        <input type="text" class="form-control" placeholder="Your surname">
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label for="last">E-mail Address</label>
                                                        <input type="email" class="form-control" placeholder="e.g tradofamily@gmail.com">
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label for="">Password</label>
                                                        <input type="password" class="form-control" id="password" placeholder="at least 8 characters for security reasons">
                                                        <div class="svx-pass-icon">
                                                            <img src="{{asset('images/eye-off.svg')}}" id="show_pass">
                                                            <img src="{{asset('images/eye-off.svg')}}" id="hide_pass">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label for="">Phone Number</label>
                                                        <div class="form-dropdown">
                                                            <div class="country-drop">
                                                                <select class="voidapt">
                                                                    <option value="NGN" class="test" data-thumbnail="{{asset('images/nigeria.svg')}}">Nigeria</option>
                                                                    <option value="ENG" data-thumbnail="{{asset('images/nigeria.svg')}}">USA (USD)</option>
                                                                    <option value="ENG" data-thumbnail="{{asset('images/nigeria.svg')}}">USA (USD)</option>
                                                                    <option value="ENG" data-thumbnail="{{asset('images/nigeria.svg')}}">USA (USD)</option>
                                                                    <option value="ENG" data-thumbnail="{{asset('images/nigeria.svg')}}">USA (USD)</option>
                                                                    <option value="ENG" data-thumbnail="{{asset('images/nigeria.svg')}}">USA (USD)</option>
                                                                </select>
                                                                <div class="lang-select">
                                                                    <button class="btn-select" type="button" value=""></button>
                                                                    <div class="b">
                                                                        <ul id="a"></ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="text" class="form-control" placeholder="811223344">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                   <div class="form-group">
                                                        <label>Referral ID(Optional)</label>
                                                        <input type="text" class="form-control">
                                                    </div> 
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>How did you hear about Us (Optional)</label>
                                                        <input type="text" class="form-control" placeholder="please select one">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-check width-ex">
                                            <label class="form-check-label" class="text-opacity">
                                                <input type="checkbox" class="form-check-input" value="checked">By creating an Account, you agree <a href="#" class="link-in">terms of Use</a> and our <a href="#" class="link-in">Privacy Policy</a>
                                            </label>
                                        </div>
                                        <button type="submit" class="btn d-block trado-pri-btn text-uppercase width-ex">Create an Account</button>
                                    </form>
                                    <div class="sin_alt">
                                        <p class="text-opacity">Already have an Account? <a href="/login" class="link-in">Log In</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
    </main>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
    <script>
        // PASSWORD TOOGLER
        function passwordShow() {
            $('#hide_pass').click(function() {
                $('#password').attr('type', 'text')
                $(this).css('display', 'none')
            });

            $('#show_pass').click(function() {
                $('#password').attr('type', 'password')
                $('#hide_pass').css('display', 'block')
            })
        }

        // PHONE DROPDOWN
        var countryArr = [];
        $('.voidapt option').each(function(){
            var img = $(this).attr("data-thumbnail");
            var value = $(this).val();
            var item = '<li><img src="'+ img +'"/></li>';
            countryArr.push(item);
        })

        $('#a').html(countryArr);
        $('.btn-select').html(countryArr[0]);
        $('.btn-select').attr('value', 'NGN');

        //CHANGE COUNTRY ON BTN ONCLICK
        $('#a li').click(function(){
            var img = $(this).find('img').attr("src");
            var value = $(this).find('img').attr('value');
            var text = this.innerText;
            var item = '<li><img src="'+ img +'" /></li>';
            $('.btn-select').html(item);
            $('.btn-select').attr('value', value);
            $(".b").toggle();
        });

        $(".btn-select").click(function(){
            $(".b").toggle();
        });

        //CHECK LOCAL STORAGE FOR THE COUNTRY
        var countries = localStorage.getItem('lang');
        if (countries){
            //find an item with value of countries
            var langIndex = countryArr.indexOf(countries);
            $('.btn-select').html(countryArr[langIndex]);
            $('.btn-select').attr('value', countries);
            } else {
            var langIndex = countryArr.indexOf('ch');
            $('.btn-select').html(countryArr[langIndex]);
        }
        passwordShow();
    </script>
</body>
</html>