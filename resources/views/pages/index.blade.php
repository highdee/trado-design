@extends('layouts.app')

@section('content')
    {{--     Carousel --}}
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="col-sm-12 p-0">
                <div class="carousel-wrap">
                    <div class="overlay"></div>
                    <div class="carousel-in">
                        <div class="carouser-txt">
                            <h2>
                                The Smartest Way
                                to Grow Money and
                                Harvest Returns
                            </h2>
                            <p>A personalized Agri-investment opportunity for everyone</p>
                        </div>
                        <div class="carousel-foot">
                            <p>Get up to 30% on Investments</p>
                            <a href="#" class="btn "> Create an Account</a>
                        </div>
                    </div>
                    <div class="carousel-banner">
                        <img src="{{asset('images/Rectangle 101@2x.png')}}" alt="">
                    </div>

                    <div class="carousel-img">
                        <img src="{{ asset('images/yellow.png') }}" width="" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--    Main Section--}}
    <div class="main container-fluid  p-0">
        {{-- financial safety --}}
        <div class="financial-wrapper p-0">
            <div class="row f-width">
                <div class="col-sm-3 col-3 col-md-2 p-0">
                    <div class="security-img">
                        <img src="images/svg/gg.png" alt="">
                    </div>
                </div>
                <div class="col-sm-9 col-9 col-md-10 p-0 f-txtWrap">
                    <h2 class="heading">
                        Your financial safety is always <br class="d-none d-md-block">
                        our priority
                    </h2>
                    <p class="f-text fin-txt">
                        Trado Global Limited uses the latest technological standards to
                        ensure financial security, and the Agriculture Insurance Corporation
                        insures all existing projects.
                    </p>
                </div>
            </div>
        </div>

        <!-- Savings -->
        <div class="savings-wrapper" >
            <div class="saving-head text-center">
                <h2 class="heading">Savings</h2>
                <p class="f-text">
                    Towards a greener financial future
                </p>
            </div>
            <div class="row saving-cards">
                <div class="col-sm-12 col-md-6 col-lg-4 crd">
                    <div class="card " >
                        <div class="card-numb">
                            <h2>01</h2>
                        </div>
                        <img src="images/event.png" class="card-img-top saving-img" alt="...">
                        <div class="card-body d-flex flex-column">
                            <h2 class="heading card-title">Your money saver</h2>
                            <p class="card-text">
                                Reach your financial objective by saving daily,
                                weekly, or monthly with a goal in mind, such
                                as retirement, vacation, down payment, or
                                other large purchases.
                            </p>
                            <a href="#" class="btn card-btn mt-auto">Learn more <span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 crd">
                    <div class="card" >
                        <div class="card-numb">
                            <h2>02</h2>
                        </div>
                        <img src="images/event.png" class="card-img-top saving-img" alt="...">
                        <div class="card-body d-flex flex-column">
                            <h5 class="heading card-title">Saving for Urgencies</h5>
                            <p class="card-text">
                                Are you looking for a secure way to build up
                                emergency funds? With Trado safe lock, you can
                                now be assured of financial security against
                                unexpected income loss while enjoying a
                                significant increase in your savings until
                                the due day.
                            </p>
                            <a href="#" class="btn card-btn mt-auto">Learn more <span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 crd">
                    <div class="card" >
                        <div class="card-numb">
                            <h2>03</h2>
                        </div>
                        <img src="images/inveto.svg" class="card-img-top inveto-img" alt="...">
                        <div class="card-body d-flex flex-column">
                            <h2 class="heading card-title">Saving towards investment</h2>
                            <p class="card-text">
                                Saving is good; investing is better
                                Enjoy financial freedom from debts by locking
                                down your three-month-old savings of
                                N20, 000 or more, to invest in low, medium risk
                                agribusiness opportunities.
                            </p>
                            <a href="#" class="btn card-btn mt-auto">Learn more <span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Farmshop -->
        <div class="farm-wrapper f-background">
            <div class="farm-head">
                <h2 class="heading2">Our Farmshop</h2>
            </div>
            <div class="row mt-0">
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 f-c">
                    <div class="farm-card">
                        <div class="farm-sec1">
                            <img class="farm-img" src="{{asset('images/asset-91.png')}}" alt="">
                            <div class="farm-details">
                                <p class="plantation">Name of Plantation </p>
                                <h5 class="plant-name">Cucumber</h5>
                                <div class="plant-cost p-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cost per unit</p>
                                        <p class="f-amt">N20,000</p>
                                    </div>
                                </div>
                                <div class="plant-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cycle duration</p>
                                        <p class="duration">6 Months</p>
                                    </div>
                                </div>
                                <span class="roi">ROI:25%</span>
                            </div>
                        </div>
                        <div class="farm-sec2">
                            <div class="farm-location">
                                <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                                <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                            </div>
                            <a href="#" class="farm-btn btn btn-success btn-lg">INVEST NOW</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 f-c">
                    <div class="farm-card">
                        <div class="farm-sec1">
                            <img class="farm-img" src="images/miguel-andrade-nAOZCYcLND8-unsplash.jpg" alt="">
                            <div class="farm-details">
                                <p class="plantation">Name of Plantation </p>
                                <h5 class="plant-name">Cucumber</h5>
                                <div class="plant-cost p-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cost per unit</p>
                                        <p class="f-amt">N20,000</p>
                                    </div>
                                </div>
                                <div class="plant-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cycle duration</p>
                                        <p class="duration">6 Months</p>
                                    </div>
                                </div>
                                <span class="roi">ROI:25%</span>
                            </div>
                        </div>
                        <div class="farm-sec2">
                            <div class="farm-location">
                                <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                                <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                            </div>
                            <a href="#" class="farm-btn btn btn-success btn-lg sold-out">Sold Out</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 f-c">
                    <div class="farm-card">
                        <div class="farm-sec1">
                            <img class="farm-img" src="images/miguel-andrade-nAOZCYcLND8-unsplash.jpg" alt="">
                            <div class="farm-details">
                                <p class="plantation">Name of Plantation </p>
                                <h5 class="plant-name">Cucumber</h5>
                                <div class="plant-cost p-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cost per unit</p>
                                        <p class="f-amt">N20,000</p>
                                    </div>
                                </div>
                                <div class="plant-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cycle duration</p>
                                        <p class="duration">6 Months</p>
                                    </div>
                                </div>
                                <span class="roi">ROI:25%</span>
                            </div>
                        </div>
                        <div class="farm-sec2">
                            <div class="farm-location">
                                <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                                <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                            </div>
                            <a href="#" class="farm-btn btn btn-success btn-lg">INVEST NOW</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 f-c">
                    <div class="farm-card">
                        <div class="farm-sec1">
                            <img class="farm-img" src="images/miguel-andrade-nAOZCYcLND8-unsplash.jpg" alt="">
                            <div class="farm-details">
                                <p class="plantation">Name of Plantation </p>
                                <h5 class="plant-name">Cucumber</h5>
                                <div class="plant-cost p-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cost per unit</p>
                                        <p class="f-amt">N20,000</p>
                                    </div>
                                </div>
                                <div class="plant-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cycle duration</p>
                                        <p class="duration">6 Months</p>
                                    </div>
                                </div>
                                <span class="roi">ROI:25%</span>
                            </div>
                        </div>
                        <div class="farm-sec2">
                            <div class="farm-location">
                                <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                                <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                            </div>
                            <a href="#" class="farm-btn btn btn-success btn-lg">INVEST NOW</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 f-c">
                    <div class="farm-card">
                        <div class="farm-sec1">
                            <img class="farm-img" src="images/miguel-andrade-nAOZCYcLND8-unsplash.jpg" alt="">
                            <div class="farm-details">
                                <p class="plantation">Name of Plantation </p>
                                <h5 class="plant-name">Cucumber</h5>
                                <div class="plant-cost p-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cost per unit</p>
                                        <p class="f-amt">N20,000</p>
                                    </div>
                                </div>
                                <div class="plant-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cycle duration</p>
                                        <p class="duration">6 Months</p>
                                    </div>
                                </div>
                                <span class="roi">ROI:25%</span>
                            </div>
                        </div>
                        <div class="farm-sec2">
                            <div class="farm-location">
                                <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                                <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                            </div>
                            <a href="#" class="farm-btn btn btn-success btn-lg">INVEST NOW</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 f-c">
                    <div class="farm-card">
                        <div class="farm-sec1">
                            <img class="farm-img" src="images/miguel-andrade-nAOZCYcLND8-unsplash.jpg" alt="">
                            <div class="farm-details">
                                <p class="plantation">Name of Plantation </p>
                                <h5 class="plant-name">Cucumber</h5>
                                <div class="plant-cost p-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cost per unit</p>
                                        <p class="f-amt">N20,000</p>
                                    </div>
                                </div>
                                <div class="plant-cost">
                                    <div class="farm-i">
                                        <img src="images/time_1.svg" alt="">
                                    </div>
                                    <div class="plant-amt">
                                        <p class="cycle">cycle duration</p>
                                        <p class="duration">6 Months</p>
                                    </div>
                                </div>
                                <span class="roi">ROI:25%</span>
                            </div>
                        </div>
                        <div class="farm-sec2">
                            <div class="farm-location">
                                <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                                <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                            </div>
                            <a href="#" class="farm-btn btn btn-success btn-lg">INVEST NOW</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="f-learnmore">
                <a href="#" class="btn ">Learn more <span class="fa fa-angle-right"></span></a>
                <div class="f-line"></div>
            </div>
        </div>

        <!-- How it Works -->
        <div class="work-wrapper farm-wrapper">
            <h2 class="heading text-center">How it works</h2>
            <p class="f-text  text-center">You are four smart steps away from growing your money</p>
            <div class="w-wrapper">

                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3 w-work">
                        <div class="row">
                            <div class="col-3 col-sm-2 col-md-12 p-0 work-numb-wrapper">
                                <div class="work">
                                    <h4 class="w-h4">01</h4>
                                </div>
                                <div class="greenline"></div>
                            </div>
                            <div class="col-9 col-sm-10 col-md-12 p-0">
                                <h5 class="w-head">Register</h5>
                                <p class="w-text">
                                    Sign up on our website with an
                                    active email address to create your
                                    personalized Trado profile.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 w-work">
                        <div class="row">
                            <div class="col-3 col-sm-2 col-md-12 p-0 work-numb-wrapper">
                                <div class="work">
                                    <h4 class="w-h4">01</h4>
                                </div>
                                <div class="greenline d-sm-block d-md-none d-lg-block"></div>
                            </div>
                            <div class="col-9 col-sm-10 col-md-12 p-0">
                                <h5 class="w-head">Check Available Monitor Investment and Invest</h5>
                                <p class="w-text">
                                    Choose what matches your
                                    investment portfolio best from our
                                    range of investment plans and make
                                    payment.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 w-work">
                        <div class="row">
                            <div class="col-3 col-sm-2 col-md-12 p-0 work-numb-wrapper">
                                <div class="work">
                                    <h4 class="w-h4">01</h4>
                                </div>
                                <div class="greenline"></div>
                            </div>
                            <div class="col-9 col-sm-10 col-md-12 p-0">
                                <h5 class="w-head">Monitor</h5>
                                <p class="w-text">
                                    After investing, you can now
                                    watch your money grow from
                                    the dashboard and receive regular
                                    emails showing current farm updates
                                    and status on your investment.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 w-work">
                        <div class="row">
                            <div class="col-3 col-sm-2 col-md-12 p-0">
                                <div class="work">
                                    <h4 class="w-h4">01</h4>
                                </div>
                            </div>
                            <div class="col-9 col-sm-10 col-md-12 p-0">
                                <h5 class="w-head">Get Paid</h5>
                                <p class="w-text">
                                    At the end of the farm cycle, enjoy
                                    investment returns plus initial
                                    investmentcapital credited to your
                                    desired account.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block-line"></div>

        <!-- transaction -->
        <div class="transaction-container farm-wrapper">
            <h5 class="transaction-h5 text-center">Insured Farms and Secured <br> transactions in partnership with</h5>
            <div class="row  text-center">
                <div class="col-6 col-sm-6 col-md-3">
                    <img src="images/naic.svg" class="naic" alt="">
                </div>
                <div class="col-6 col-sm-6 col-md-3">
                    <img src="images/download.svg" class="transaction-img" alt="">
                </div>
                <div class="col-6 col-sm-6 col-md-3">
                    <img src="images/visa.svg" class="visa" alt="">
                </div>
                <div class="col-6 col-sm-6 col-md-3">
                    <img src="images/download.svg" class="transaction-img" alt="">
                </div>
            </div>
        </div>

        <!-- Social Impact -->
        <div class="social-wrapper" >
            <div class="social-layout"></div>
            <div class="social-content farm-wrapper">
                <div class="social-head  text-center">
                    <h2 class="heading">Our Social Impact</h2>
                    <p class="f-text">We are dedicated to improving the standard of living for everyone.</p>
                </div>
                <div class="row social">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="social-inner">
                            <div class="social-img">
                                <img src="images/businessman.svg" alt="">
                            </div>
                            <div class="social-description">
                                <h5 class="s-total">137 <span>+</span></h5>
                                <span class="s-name">Acres farmed</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="social-inner">
                            <div class="social-img">
                                <img src="images/businessman.svg" alt="">
                            </div>
                            <div class="social-description">
                                <h5 class="s-total">137 <span>+</span></h5>
                                <span class="s-name">Acres farmed</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="social-inner">
                            <div class="social-img">
                                <img src="images/farmer.svg" alt="">
                            </div>
                            <div class="social-description">
                                <h5 class="s-total">137 <span>+</span></h5>
                                <span class="s-name">Acres farmed</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="social-inner">
                            <div class="social-img">
                                <img src="images/businessman.svg" alt="">
                            </div>
                            <div class="social-description">
                                <h5 class="s-total">100000 <span>+</span></h5>
                                <span class="s-name">Acres farmed</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- what our smart users are saying --}}
        <div class="row users-wrapper">
            <div class="col-md-12 text-text  farm-wrapper">
                <h2 class="heading">Here's what our <br> smart users are saying</h2>
            </div>
            <div class="col-md-12 owl-col">
                <div class="user-container owl-carousel owl-theme">
                    <div class="item">
                        <div class="users-box">
                            <div class="user-pro">
                                <div class="user-image">
                                    <div class="user-pic" id="user-pic1"></div>
                                </div>
                                <h5 class="user-name">Mahnager</h5>
                            </div>
                            <div class="user-text">
                                <p>
                                    The best Agricultural platform I have seen,
                                    even paying their investors before
                                    the Agreed date .Also compensating their
                                    investors with customized T-shirt
                                </p>
                            </div>
                            <div class="user-line"></div>
                            <p class="user-duration">Posted 2 months ago</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="users-box">
                            <div class="user-pro">
                                <div class="user-image">
                                    <div class="user-pic" id="user-pic2"></div>
                                </div>
                                <h5 class="user-name">Mahnager</h5>
                            </div>
                            <div class="user-text">
                                <p>
                                    The best platform to invest your money
                                    is no other than Tradoglobal
                                </p>
                            </div>
                            <div class="user-line"></div>
                            <p class="user-duration">Posted 2 months ago</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="users-box">
                            <div class="user-pro">
                                <div class="user-image">
                                    <div class="user-pic" id="user-pic2"></div>
                                </div>
                                <h5 class="user-name">Mahnager</h5>
                            </div>
                            <div class="user-text">
                                <p>
                                    The best Agricultural platform I have seen,
                                    even paying their investors before
                                    the Agreed date .Also compensating their
                                    investors with customized T-shirt
                                </p>
                            </div>
                            <div class="user-line"></div>
                            <p class="user-duration">Posted 2 months ago</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="users-box">
                            <div class="user-pro">
                                <div class="user-image">
                                    <div class="user-pic" id="user-pic2" ></div>
                                </div>
                                <h5 class="user-name">Mahnager</h5>
                            </div>
                            <div class="user-text">
                                <p>
                                    The best platform to invest your money
                                    is no other than Tradoglobal
                                </p>
                            </div>
                            <div class="user-line"></div>
                            <p class="user-duration">Posted 2 months ago</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="users-box">
                            <div class="user-pro">
                                <div class="user-image">
                                    <div class="user-pic" id="user-pic2"></div>
                                </div>
                                <h5 class="user-name">Mahnager</h5>
                            </div>
                            <div class="user-text">
                                <p>
                                    The best Agricultural platform I have seen,
                                    even paying their investors before
                                    the Agreed date .Also compensating their
                                    investors with customized T-shirt
                                </p>
                            </div>
                            <div class="user-line"></div>
                            <p class="user-duration">Posted 2 months ago</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="users-box">
                            <div class="user-pro">
                                <div class="user-image">
                                    <div class="user-pic" id="user-pic2"></div>
                                </div>
                                <h5 class="user-name">Mahnager</h5>
                            </div>
                            <div class="user-text">
                                <p>
                                    The best platform to invest your money
                                    is no other than Tradoglobal
                                </p>
                            </div>
                            <div class="user-line"></div>
                            <p class="user-duration">Posted 2 months ago</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {{--  blog --}}
        <div class="blog-wrapper farm-wrapper">
            <div class="row">
                <div class="col-3 col-sm-3 col-md-3">
                    <img src="{{asset('images/AJIP.jpg')}}" class="blog-image" alt="">
                </div>
                <div class="col-9 col-sm-9 col-md-9 p-0">
                    <div class="blog-content">
                        <h5>Blog trends</h5>
                        <p>
                            Trado Global limited harvests her first farm plantation <br class="d-none d-md-block"> as she paid out Okra and Pepper Investors <br class="d-none d-md-block"> <span class="blog-p">N12,775,000 </span>in Naira
                        </p>

                    </div>
                </div>
                <div class="col-12">
                    <div class="f-learnmore b-learn">
                        <a href="#">Learn more <span class="fa fa-angle-right"></span></a>
                        <div class="f-line"></div>
                    </div>
                </div>
            </div>
        </div>

        {{--  download --}}
        <div class="download-wrapper farm-wrapper">
            <h2 class="d-heading">Taking the Hassles away  of  Growing your Money</h2>
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h5 class="downloadh5">Download the app on Google Play or the App Store</h5>
                    <p class="download-p">
                        With the app on your smartphone, you have your financial
                        freedom at your fingertip. However you wish to handle your
                        money, you can do it from the app, no boundaries.
                        Click your money hassles.
                    </p>
                    <a href="#" class="btn btn-success create-acc btn-lg">Create an Account</a>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="{{asset('images/google-play.png')}}" alt="" class="google-play">
                    <img src="{{asset('images/apple-store.png')}}" alt="" class="apple-store">
                </div>

            </div>
        </div>
        <div class="block-line"></div>

        {{-- Smart Money--}}
        <div class="smartMoney-wrapper farm-wrapper">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="s-content">
                        <h2 class="s-heading">Become a Smart Money Insider</h2>
                        <p class="s-text">
                            Subscribe to our must-read newsletter and be in the know for the latest
                            agribusiness opportunities.
                        </p>
                    </div>
                </div>
                <div class="col-md-12">
                    <form action="">
                        <div class="frm-grp">
                            <input type="text" placeholder="Example@gmail.com">
                            <button type="submit" class="btn smart-btn">
                                <span class="fa fa-paper-plane"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
