<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- BS4 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&family=Roboto:wght@400;500;700;900&display=swap"
        rel="stylesheet">
    <!-- Style Sheet -->
    <link rel="stylesheet" href="{{asset('css/user_utilities.css')}}">
</head>

<body>
    <main>
       <div class="wrapper">
            <div class="image-intro-container h-100">
                <div class="trd-js32 trd-js442">
                    <div class="abs abs-trd-xl">
                        <div class="top mb-4">
                            <div class="top-brand">
                                <img src="{{ asset('images/trado-logo.png') }}" alt="_logo">
                            </div>
                            <div class="header trd-js434-top">
                                <h5>Forgot Password</h2>
                                <p class="o">No worries, let's get you logged in</p>
                            </div>
                        </div>
                        <div class="trd-js434">
                            <img src="{{asset('/images/forgot_pass.svg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <!-- SIGN UP CONTAINER -->
            <div class="container h-max-content">
                <div class="row">
                    <div class="col-12 trd-1112">
                        <div class="trd-1112-bg"></div>
                        <div class="trd-1112-wrapper">              
                            <div class="trd-1112-top trd-xvz">
                                <div class="top-img">
                                    <img src="{{asset('images/trado_white.png')}}" alt="_logo">
                                </div>
                            </div>
                            <div class="trd-1112-form trd-xvz tr-pd w-100">
                                <div class="header trd-js-lg">
                                    <h5 class="mb-3">Forgot Password?</h2>
                                    <img src="{{asset('/images/forgot_password.png')}}" alt="" class="img-fluid">
                                </div>
                                <div class="sign_up_form">
                                    <form novalidate>
                                        <div class="container px-0">
                                            <div class="row">
                                                <div class="col-12 mb-2">
                                                    <div class="form-group trad-js-734">
                                                        <label for="last">E-mail Address</label>
                                                        <input type="email" class="form-control trad-h-100" placeholder="kindly input your registered e-mail Address">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn d-block trado-pri-btn text-uppercase width-ex w-100">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
    </main>
</body>

</html>