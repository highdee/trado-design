@extends('layouts.app')

@section('content')
    <div class="container-fluid  p-o" id="blog-container">
        <div class="row blogWrapper">
            <div class="col-12">
                <h2 class="blog-heading">
                    Our Blog
                </h2>
            </div>
        </div>
        <div class="row blogCard-wrapper">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 p-0">
                <div class="card blog-card" id="b-card1" style="width: 18rem;">
                    <div class="blogcrd">
                        <img src="{{asset('images/AJIP.jpg')}}" class="card-img-top" alt="">
                    </div>
                    <div class="card-body blog-cardBody">
                        <p class="card-text blog-cardtext">Trado Global limited harvests her first farm
                            plantation as she paid out Okra and
                            Pepper Investors N12,775,000 in Naira</p>
                        <div class="b-learnmore">
                            <a href="#" class="btn btn-default">Read more <span class="fa fa-angle-right"></span></a>
                            <div class="blog-line"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 p-0">
                <div class="card blog-card" style="width: 18rem;">
                    <div class="blogcrd">
                        <img src="{{asset('images/LTAB2.jpg')}}" class="card-img-top" alt="">
                    </div>
                    <div class="card-body blog-cardBody">
                        <p class="card-text blog-cardtext">Trado Global limited harvests her first farm
                            plantation as she paid out Okra and
                            Pepper Investors N12,775,000 in Naira</p>
                        <div class="b-learnmore">
                            <a href="#" class="btn btn-default">Read more <span class="fa fa-angle-right"></span></a>
                            <div class="blog-line"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 p-0">
                <div class="card blog-card" style="width: 18rem;">
                    <div class="blogcrd">
                        <img src="{{asset('images/AGB1.jpg')}}" class="card-img-top" alt="">
                    </div>
                    <div class="card-body blog-cardBody">
                        <p class="card-text blog-cardtext">Trado Global limited harvests her first farm
                            plantation as she paid out Okra and
                            Pepper Investors N12,775,000 in Naira</p>
                        <div class="b-learnmore">
                            <a href="#" class="btn btn-default">Read more <span class="fa fa-angle-right"></span></a>
                            <div class="blog-line"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 p-0">
                <div class="card blog-card" style="width: 18rem;">
                    <div class="blogcrd">
                        <img src="{{asset('images/BPTr.jpg')}}" class="card-img-top" alt="">
                    </div>
                    <div class="card-body blog-cardBody">
                        <p class="card-text blog-cardtext">Trado Global limited harvests her first farm
                            plantation as she paid out Okra and
                            Pepper Investors N12,775,000 in Naira</p>
                        <div class="b-learnmore">
                            <a href="#" class="btn btn-default">Read more <span class="fa fa-angle-right"></span></a>
                            <div class="blog-line"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 p-0">
                <div class="card blog-card" style="width: 18rem;">
                    <div class="blogcrd">
                        <img src="{{asset('images/LTAB2.jpg')}}" class="card-img-top" alt="">
                    </div>
                    <div class="card-body blog-cardBody">
                        <p class="card-text blog-cardtext">Trado Global limited harvests her first farm
                            plantation as she paid out Okra and
                            Pepper Investors N12,775,000 in Naira</p>
                        <div class="b-learnmore">
                            <a href="#" class="btn btn-default">Read more <span class="fa fa-angle-right"></span></a>
                            <div class="blog-line"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 p-0">
                <div class="card blog-card" style="width: 18rem;">
                    <div class="blogcrd">
                        <img src="{{asset('images/LTAB2.jpg')}}" class="card-img-top" alt="">
                    </div>
                    <div class="card-body blog-cardBody">
                        <p class="card-text blog-cardtext">Trado Global limited harvests her first farm
                            plantation as she paid out Okra and
                            Pepper Investors N12,775,000 in Naira</p>
                        <div class="b-learnmore">
                            <a href="#" class="btn btn-default">Read more <span class="fa fa-angle-right"></span></a>
                            <div class="blog-line"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
