<!-- Navbar -->
<div class="container-fluid navbar-wrapper p-0 width">
   <div class="row footer-wrapper farm-wrapper">
       <div class="col-sm-12 col-md-12 col-lg-5">
           <img src="{{asset('images/TRADO COLOR.png')}}" class="foot-img" alt="">
           <div class="foot-contact">
               <p class="foot-location">
                   No. 10 Obafemi Awolowo Way,
                   Treasure plaza Igbona, Oshogbo,
                   Osun State
               </p>
               <div class="f-locateNumb">
                   <p class="foot-numb">+2349034484541</p>
                   <p class="f-url">info@trado.ng</p>
               </div>
           </div>
       </div>
       <div class="col-6 col-sm-6 col-md-4">
           <div class="foot-head">
               <h5>Company</h5>
           </div>
           <ul class="foot-links">
               <li>
                   <a href="#" class="foot-link">About Us</a>
               </li>
               <li>
                   <a href="#" class="foot-link">FAQs</a>
               </li>
               <li>
                   <a href="#" class="foot-link">Terms and Conditions</a>
               </li>
               <li>
                   <a href="#" class="foot-link">Privacy policy</a>
               </li>
           </ul>
       </div>
       <div class="col-6 col-sm-6 col-md-3">
           <div class="foot-head">
               <h5>Follow Us</h5>
           </div>
           <ul class="foot-links">
               <li>
                   <img src="{{asset('images/facebook.svg')}}" alt="">
                   <a href="#" class="foot-link foot-social">Facebook</a>
               </li>
               <li>
                   <img src="{{asset('images/facebook.svg')}}" alt="">
                   <a href="#" class="foot-link foot-social">Twitter</a>
               </li>
               <li>
                   <img src="{{asset('images/facebook.svg')}}" alt="">
                   <a href="#" class="foot-link foot-social">Instagram</a>
               </li>
               <li>
                   <img src="{{asset('images/facebook.svg')}}" alt="">
                   <a href="#" class="foot-link foot-social">Linkedln</a>
               </li>
           </ul>
       </div>

   </div>
</div>
<div class="foot2 d-none d-md-block">

</div>
