<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- BS4 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&family=Roboto:wght@400;500;700;900&display=swap"
        rel="stylesheet">
    <!-- Style Sheet -->
    <link rel="stylesheet" href="{{asset('css/user_utilities.css')}}">
</head>

<body>
    <main>
       <div class="wrapper">
            <div class="image-intro-container h-100">
                <div class="trd-js32 trd-js-flex">
                    <div class="abs">
                        <div class="top">
                            <div class="top-brand">
                                <img src="{{ asset('images/trado-logo.png') }}" alt="_logo">
                            </div>
                            <div class="header trd-js434-top">
                                <h5>Welcome Back <br> Smart Money Investor</h5>
                            </div>
                        </div>
                        <div class="trd-js434 trd-js445">
                            <img src="{{ asset('images/home3.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <!-- SIGN UP CONTAINER -->
            <div class="container h-100 h-max-content">
                <div class="row h-100">
                    <div class="col-12 trd-1112 h-100">
                        <div class="trd-1112-bg"></div>
                        <div class="trd-1112-wrapper">
                            <div class="trd-1112-top trd-xvz mt-0">
                                <div class="top-img">
                                    <img src="{{asset('images/trado_white.png')}}" alt="_logo">
                                </div>
                            </div>
                            <div class="trd-1112-form trd-xvz tr-pd">
                                <div class="header trd-js-lg">
                                    <h5>Welcome Back <br> Smart Money Investor</h2>
                                </div>
                                <div class="sign_up_form">
                                    <form novalidate>
                                        <div class="container px-0">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group trad-js-734">
                                                        <label for="last">E-mail Address</label>
                                                        <input type="email" class="form-control" placeholder="e.g tradofamily@gmail.com">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group trad-js-734">
                                                        <label for="">Password</label>
                                                        <input type="password" class="form-control" id="password" placeholder="at least 8 characters for security reasons">
                                                        <div class="svx-pass-icon bottom-icon">
                                                            <img src="{{asset('images/eye-off.svg')}}" id="show_pass">
                                                            <img src="{{asset('images/eye-off.svg')}}" id="hide_pass">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-check width-ex w-100">
                                            <label class="form-check-label text-opacity trado-w-js0453">
                                                <span><input type="checkbox" class="form-check-input" value="checked">remember</span>
                                                <a href="/forgot-password" class="link-in jsa-float">Forgot Password</a>
                                            </label>
                                        </div>
                                        <button type="submit" class="btn d-block trado-pri-btn text-uppercase width-ex w-100">Log In</button>
                                    </form>
                                    <div class="sin_alt">
                                        <p class="text-opacity">Do not have an account? <a href="/sign-up" class="link-in">Create One</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
    </main>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
    <script>
        // PASSWORD TOOGLER
        (function passwordShow() {
            $('#hide_pass').click(function() {
                $('#password').attr('type', 'text')
                $(this).css('display', 'none')
            });

            $('#show_pass').click(function() {
                $('#password').attr('type', 'password')
                $('#hide_pass').css('display', 'block')
            })
        })()
    </script>
</body>

</html>