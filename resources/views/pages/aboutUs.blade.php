@extends('layouts.app')

@section('content')
    <div class="container-fluid aboutUs-wrapper p-0">
        <div class="aboutUs-bg">
            <div class="about-cover"></div>
            <h2 class="abtUs-heading">About Us</h2>
        </div>
        <div class="row trado-wrapper">
            <div class="col-md-12 col-lg-7">
                <div class="tradoBlobal-wrapper">
                    <h2 class="abt-H2">
                        Trado Global Limited is a financial agribusiness investment platform
                        focused on agriculture and SDGs
                    </h2>
                    <p class="abt-p">
                        As an agribusiness platform whose core values
                        are human-centric amongst which the sustainable
                        development goals rank first. Our goal is to increase
                        the living standards of every low-income farmer and
                        the everyday Nigerian by connecting farmers and investors
                        to better agribusiness opportunities to grow impact
                        funds and earn smart returns.
                    </p>
                </div>
            </div>
            <div class="  col-lg-5 okro-pep">
                <div class="veg-wrapper">
                    <div class="pepper-bg">
                        <div class="pepper-img " id="pep-img" ></div>
                    </div>
                    <div class="okro-img" >
                        <div class="" id="okro-bg"></div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Our Mission--}}
        <div class="row mission-wrapper p-0 m-0">
            <div class="target-wrapper">
                <div class="target-img">
                    <img src="{{asset('images/target (1).svg')}}" class="target" alt="">
                </div>
            </div>
            <div class="mission-wrap">
                <h2 class="abtUs-heading" id="mission-head">Our Mission</h2>
                <p class="abt-p" id="abt-p">
                    Our mission is to invest in digital innovation and technologies to swiftly transform
                    our agriculture and food supply chain while sponsoring low-income farmers,
                    creating an alternative wealth creation, and ensuring access to modern-day
                    financial services, all from the comfort of your home.
                </p>
            </div>
        </div>

        {{-- Company's Team --}}
        <div class="row Companywrapper trado-wrapper">
            <div class="col-sm-12 col-lg-6">
                <div class="teamImg-bg">
{{--                    <div class="team-img" ></div>--}}
                    <img src="{{asset('images/teamImage (3).png')}}" class="team-img" alt="">
                </div>
            </div>
            <div class="col-sm-12 col-lg-6">
                <h2 class="abt-H2">
                    The company consists of a
                    dedicated team of agritech
                    professionals,
                </h2>
                <p class="abt-p">
                    with the best prove of expertise in their various
                    fields of endeavor providing the best insights and advice on
                    the most sustainable agricultural practices to ensure the best
                    profits and efficiencies
                </p>
            </div>
        </div>

        {{-- Our Team--}}
        <div class="ourTeam-container">
            <div class="ourTeam-wrapper">
            <div class="team-txt">
                <h3 class="team-h3">OUR TEAM</h3>
            </div>
            <div class="row team-profiles">
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <div class="team-profile">
                        <div class="teamBg-box">
                            <div class="teamBg-img" id="t-bg1"></div>
                            <div class="team-line"></div>
                        </div>
                        <h6 class=" team-name">Keefa Umar</h6>
                        <p class="team-title">(CEO)</p>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-3">
                        <div class="team-profile">
                            <div class="teamBg-box">
                                <div class="teamBg-img" id="t-bg2"></div>
                                <div class="team-line"></div>
                            </div>
                            <h6 class=" team-name">Keefa Umar</h6>
                            <p class="team-title">(CEO)</p>
                        </div>
                    </div>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <div class="team-profile">
                        <div class="teamBg-box">
                            <div class="teamBg-img" id="t-bg1"></div>
                            <div class="team-line"></div>
                        </div>
                        <h6 class=" team-name">Keefa Umar</h6>
                        <p class="team-title">(CEO)</p>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <div class="team-profile">
                        <div class="teamBg-box">
                            <div class="teamBg-img" id="t-bg2"></div>
                            <div class="team-line"></div>
                        </div>
                        <h6 class=" team-name">Keefa Umar</h6>
                        <p class="team-title">(CEO)</p>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <div class="team-profile">
                        <div class="teamBg-box">
                            <div class="teamBg-img" id="t-bg1"></div>
                            <div class="team-line"></div>
                        </div>
                        <h6 class=" team-name">Keefa Umar</h6>
                        <p class="team-title">(CEO)</p>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-3">
                        <div class="team-profile">
                            <div class="teamBg-box">
                                <div class="teamBg-img" id="t-bg2"></div>
                                <div class="team-line"></div>
                            </div>
                            <h6 class=" team-name">Keefa Umar</h6>
                            <p class="team-title">(CEO)</p>
                        </div>
                    </div>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <div class="team-profile">
                        <div class="teamBg-box">
                            <div class="teamBg-img" id="t-bg1"></div>
                            <div class="team-line"></div>
                        </div>
                        <h6 class=" team-name">Keefa Umar</h6>
                        <p class="team-title">(CEO)</p>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <div class="team-profile">
                        <div class="teamBg-box">
                            <div class="teamBg-img" id="t-bg2"></div>
                            <div class="team-line"></div>
                        </div>
                        <h6 class=" team-name">Keefa Umar</h6>
                        <p class="team-title">(CEO)</p>
                    </div>
                </div>
            </div>
        </div>
        </div>

        {{-- What Makes Us Unique--}}
        <div class="unique-wrapper trado-wrapper">
            <div class="row">
                <div class="col-sm-12 col-lg-8">
                    <div class="uniqueTxt-wrapper">
                        <h2 class="unique-h2">What Makes Us Unique</h2>
                        <div class="uniqueTxt-box Utxt" >
                            <img src="{{asset('images/accept_cr.svg')}}" alt="">
                            <p class="abt-p unique-p">
                                <span class="unique-yellow">Two years strong!</span> Our rates are unmatched
                                because the interest of our investors is our priority
                            </p>
                        </div>
                        <div class="uniqueTxt-box Utxt">
                            <img src="{{asset('images/accept_cr.svg')}}" alt="">
                            <p class="abt-p unique-p">
                                Trado Global Limited stands out of the crowd
                                because we are the farmers, not mere intermediaries.
                            </p>
                        </div>
                        <div class="uniqueTxt-box">
                            <img src="{{asset('images/accept_cr.svg')}}" alt="">
                            <p class="abt-p unique-p">
                                With the use of modern farming skills and techniques, technologically
                                improved seeds, and farm machinery, we are committed to expanding
                                our reach in improving living standards for all.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">
                    <div class="uniqueImg-wrapper">
                        <div class="unique-bg" id="unique-bg1" >
                            <div class="unique-img1"></div>
                        </div>
                        <div class="unique-bg" id="unique-bg2" >
                            <div class="unique-img2"></div>
{{--                            <img class="unique-img1" src="{{asset('images/EZwqF6dXgAE0dbP.jpeg')}}">--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--        Image Section--}}
        <div class="image-wrapper">
            <div class="image-container">
                <div class="row unique-wrapper">
                    <div class="col-sm-12 col-md-7">
                        <div class="imageSec-box">
                            <div class="image-border" id="img-container1">
                                <img src="{{asset('images/EcqK3GfXsAAIu1i.jpeg')}}" class="img-fluid"   alt="">
                            </div>
                            <div class="image-border" id="img-container2">
                                <img src="{{asset('images/EjQ9nnzXsAIPTYN.jpeg')}}" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="imageBtn-wrapper">
                            <div class="image-box3 image-border">
                                <img src="{{asset('images/EZQsFxAWkAEkUQY.jpeg')}}" alt="">
                            </div>
                            <a href="#" class="btn btnsuccess image-a">Create an Account</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Phone section--}}
        <div class="row phone-wrapper trado-wrapper">
            <div class="col-sm-12 col-md-5 col-lg-5">
                <div class="phoneImg-wrapper">
                    <img src="{{asset('images/phone 2.svg')}}" class="phone-img" alt="">
                </div>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7">
                <div class="phoneText-wrapper">
                    <h2 class="phone-txt abt-H2">
                        Join other Nigerians who are
                        growing impact funds remotely
                        today and earn smart tomorrow!
                    </h2>
                    <p class="phone-p abt-p">
                        With the app on your smartphone, you have your financial freedom
                        at your fingertip. However you wish to handle your money, you can
                        do it from the app, no boundaries. Click your money hassles. Bye
                    </p>
                    <div class="phone-png">
                        <img src="{{asset('images/google-play.png')}}" alt="">
                        <img src="{{asset('images/google-play.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
