@extends('layouts.dashboard')

@section('content')
    <main>
        <div class="trd-invs-wrapper pb-2">
            <div class="trd-invs-top">
                <div class="farmshop-header investment mb-0">
                    <div class="farmhead-txt-wrapper">
                        <span>
                            <a href="javascript:void(0)"><img src="{{ asset('/images/angle-invs-right.png') }}"></a>
                        </span>
                    </div>
                    <div>
                        <img src="{{asset('images/asset-131.png')}}" class="farmhead-img" alt="">
                    </div>
                </div>
                 <div class="sellers-table-list">
                    <div class="d-xl-none">
                        <img src="{{ asset('/images/sellers_ls.png') }}">
                        <span>see Seller's list</span>
                    </div>
                    <div class="seller-dsh-links d-none d-xl-block">
                        <a href="/user/dashboard">dashboard> </a>
                        <a href="/user/dashboard/transaction/investments">investment> </a>
                        <b>Seller's List</b>
                    </div>
                </div>
                <div class="topinvestments">
                    <div class="table-sellers-list">
                        <div class="d-none d-xl-flex table-list-xl">
                            <div class="xl-sellers-list">
                                <img src="{{ asset('/images/sellers_ls.png') }}">
                                <span>see Seller's list</span>
                            </div>
                            <div class="hide-sellers">
                                 <a href="javascript:void(0)" class="btn">
                                    <img src="{{ asset('/images/close-icon.png') }}">
                                </a>
                            </div>
                        </div>
                        <table class="table table-list">
                             <thead>
                                <tr>
                                    <th>Plantation Type</th>
                                    <th>Time Frame</th>
                                    <th>Amount</th>
                                    <th class="d-none d-xl-inline-block">Units</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                             <tbody>
                                <tr>
                                    <td>Pineapple (3 units)</td>
                                    <td>2 Months <span class="d-none d-xl-inline">Remaining</span></td>
                                    <td>N75,000</td>
                                    <td class="d-none d-xl-inline-block">3</td>
                                    <td>No buyer yet</td>
                                    <td>
                                        <a href="javascript:void(0)" class="btn d-xl-none">
                                            <img src="{{ asset('/images/close-icon.png') }}">
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-sellers d-none d-xl-block">cancel</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Habanero (8 units)</td>
                                    <td>3 Months <span class="d-none d-xl-inline">Remaining</span></td>
                                    <td>N125,000</td>
                                     <td class="d-none d-xl-inline-block">8</td>
                                    <td>No buyer yet</td>
                                    <td>
                                        <a href="javascript:void(0)" class="btn d-xl-none">
                                            <img src="{{ asset('/images/close-icon.png') }}">
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-sellers d-none d-xl-block">cancel</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Pepper (8 units)</td>
                                    <td>1 Month <span class="d-none d-xl-inline">Remaining</span></td>
                                    <td>N15,000</td>
                                     <td class="d-none d-xl-inline-block">10</td>
                                    <td>No buyer yet</td>
                                    <td>
                                        <a href="javascript:void(0)" class="btn d-xl-none">
                                            <img src="{{ asset('/images/close-icon.png') }}">
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-sellers d-none d-xl-block">cancel</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                   </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('script')
    
@endsection
