@extends('layouts.dashboard')

@section('content')
    <main>
        <!-- TRANSACTIONS T-->
        <div class="farmshop-header transctions">
            <div class="farmhead-txt-wrapper">
                <h4 class="farm-h4">Transactions</h4>
            </div>
            <div>
                <img src="{{asset('images/asset-131.png')}}" class="farmhead-img" alt="">
            </div>
        </div>
        <div class="farmshop-dash">
            <p class="frm-dash-p">Dashboard <span class="fa fa-angle-right"></span></p>
            <p class="frm-dash-p2">Transactions</p>
        </div>
        <div class="container-fluid">
            <div class="row mt-0 section-wrapper">
                <div class="col-12">
                    <div class="trasnction-tabs">
                         <div class="transaction-tab">
                            <a href="javascript:void(0)" class="btn tabs-btn active">All</a>
                            <a href="/user/dashboard/savings" class="btn tabs-btn">Savings</a>
                            <a href="javascript:void(0)" class="btn tabs-btn">Widthdrawal</a>
                            <a href="/user/dashboard/transaction/investments" class="btn tabs-btn">Investments</a>
                        </div>
                        <div class="trsct-table-header">
                            <h6 class="mb-0">Today</h6>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="trsct-table">
                        <div id="transc-today" class="table-flex">
                            <div class="table-view">
                                <div class="table-header">
                                    <span class="table-head-content">
                                        <span>Title</span>
                                        <span>Refrence ID</span>
                                        <span>Date/Time</span>
                                        <span>Amount</span>
                                    </span>
                                </div>
                                <div class="table-body">
                                    <div class="table-row">
                                        <span class="table-data">
                                            <span scope="row" class="tb-item">
                                                <div class="table-items">
                                                    <span class="item-img type-1">
                                                        <img src="{{ asset('images/savings.svg') }}" alt="">
                                                    </span>
                                                    <span class="item-tag">
                                                        <h5>Investments</h5>
                                                        <p>Pineapple Plantation</p>
                                                    </span>
                                                </div>
                                            </span>
                                            <span class="tb-item">4567890665432567fb9</span>
                                            <span class="tb-item">Sept. 16, 2020/ 5:00pm</span>
                                            <span class="tb-item">
                                                <div class="details type-y">
                                                    <h5 class="price">N5,000</h5>
                                                    <p class="unit">25% ROI (6 units)</p>
                                                </div>
                                            </span>
                                        </span>
                                        <span class="table-data">
                                            <span scope="row" class="tb-item">
                                                <div class="table-items">
                                                    <span class="item-img">
                                                        <img src="{{ asset('images/savings.svg') }}" alt="">
                                                    </span>
                                                    <span class="item-tag">
                                                        <h5>Widthdrawal</h5>
                                                        <p>Mango Plantation</p>
                                                    </span>
                                                </div>
                                            </span>
                                            <span class="tb-item">3467890665432567fb9</span>
                                            <span class="tb-item">Sept. 16, 2020/ 5:00pm</span>
                                            <span class="tb-item">
                                                <div class="details type-x">
                                                    <h5 class="price">N5,000</h5>
                                                    <p class="unit">25% ROI (6 units)</p>
                                                </div>
                                            </span>
                                        </span>
                                        <span class="table-data">
                                            <span scope="row" class="tb-item">
                                                    <div class="table-items">
                                                        <span class="item-img">
                                                            <img src="{{ asset('images/savings.svg') }}" alt="">
                                                        </span>
                                                        <span class="item-tag">
                                                            <h5>Widthdrawal</h5>
                                                            <p>Pawpaw Plantation</p>
                                                        </span>
                                                    </div>
                                                </span>
                                                <span class="tb-item">3422890665432567fb9</span>
                                                <span class="tb-item">Sept. 16, 2020/ 5:00pm</span>
                                                <span class="tb-item">
                                                    <div class="details type-x">
                                                        <h5 class="price">N5,000</h5>
                                                        <p class="unit">25% ROI (6 units)</p>
                                                    </div>
                                                </span>
                                            </span>
                                        </span>
                                        <span class="table-data">
                                            <span scope="row" class="tb-item">
                                                <div class="table-items">
                                                    <span class="item-img type-1">
                                                        <img src="{{ asset('images/savings.svg') }}" alt="">
                                                    </span>
                                                    <span class="item-tag">
                                                        <h5>Investments</h5>
                                                        <p>Habanero Plantation</p>
                                                    </span>
                                                </div>
                                            </span>
                                            <span class="tb-item">2234890665432567fb9</span>
                                            <span class="tb-item">Sept. 16, 2020/ 5:00pm</span>
                                            <span class="tb-item">
                                                <div class="details type-y">
                                                    <h5 class="price">N5,000</h5>
                                                    <p class="date">Aug. 5, 2020</p>
                                                </div>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="transc-today" class="table-flex padding-bottom-tab">
                            <div class="table-header">
                                <span class="table-head-content">
                                    <span class="table-tab-top">This Week</span>
                                </span>
                            </div>
                            <div class="table-view">
                                <div class="table-body">
                                    <div class="table-row">
                                        <span class="table-data">
                                            <span scope="row" class="tb-item">
                                                <div class="table-items">
                                                    <span class="item-img type-1">
                                                        <img src="{{ asset('images/savings.svg') }}" alt="">
                                                    </span>
                                                    <span class="item-tag">
                                                        <h5>Investments</h5>
                                                        <p>Pineapple Plantation</p>
                                                    </span>
                                                </div>
                                            </span>
                                            <span class="tb-item">4567890665432567fb9</span>
                                            <span class="tb-item">Sept. 16, 2020/ 5:00pm</span>
                                            <span class="tb-item">
                                                <div class="details type-y">
                                                    <h5 class="price">N5,000</h5>
                                                    <p class="unit">25% ROI (6 units)</p>
                                                </div>
                                            </span>
                                        </span>
                                        <span class="table-data">
                                            <span scope="row" class="tb-item">
                                                <div class="table-items">
                                                    <span class="item-img">
                                                        <img src="{{ asset('images/savings.svg') }}" alt="">
                                                    </span>
                                                    <span class="item-tag">
                                                        <h5>Widthdrawal</h5>
                                                        <p>Mango Plantation</p>
                                                    </span>
                                                </div>
                                            </span>
                                            <span class="tb-item">3467890665432567fb9</span>
                                            <span class="tb-item">Sept. 16, 2020/ 5:00pm</span>
                                            <span class="tb-item">
                                                <div class="details type-x">
                                                    <h5 class="price">N5,000</h5>
                                                    <p class="unit">25% ROI (6 units)</p>
                                                </div>
                                            </span>
                                        </span>
                                        <span class="table-data">
                                            <span scope="row" class="tb-item">
                                                    <div class="table-items">
                                                        <span class="item-img">
                                                            <img src="{{ asset('images/savings.svg') }}" alt="">
                                                        </span>
                                                        <span class="item-tag">
                                                            <h5>Widthdrawal</h5>
                                                            <p>Pawpaw Plantation</p>
                                                        </span>
                                                    </div>
                                                </span>
                                                <span class="tb-item">3422890665432567fb9</span>
                                                <span class="tb-item">Sept. 16, 2020/ 5:00pm</span>
                                                <span class="tb-item">
                                                    <div class="details type-x">
                                                        <h5 class="price">N5,000</h5>
                                                        <p class="unit">25% ROI (6 units)</p>
                                                    </div>
                                                </span>
                                            </span>
                                        </span>
                                        <span class="table-data">
                                            <span scope="row" class="tb-item">
                                                <div class="table-items">
                                                    <span class="item-img type-1">
                                                        <img src="{{ asset('images/savings.svg') }}" alt="">
                                                    </span>
                                                    <span class="item-tag">
                                                        <h5>Investments</h5>
                                                        <p>Habanero Plantation</p>
                                                    </span>
                                                </div>
                                            </span>
                                            <span class="tb-item">2234890665432567fb9</span>
                                            <span class="tb-item">Sept. 16, 2020/ 5:00pm</span>
                                            <span class="tb-item">
                                                <div class="details type-y">
                                                    <h5 class="price">N5,000</h5>
                                                    <p class="date">Aug. 5, 2020</p>
                                                </div>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
