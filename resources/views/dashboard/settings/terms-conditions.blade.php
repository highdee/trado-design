<div class="edit-header-wrapper" id="chgPwd-header-wrapper">
    <div class="set-angle-left">
        <i class="fa fa-angle-left tab-arrow-backImg"></i>
    </div>
    <div class="edit-header" id="terms-header">
        <h4 class="settings-h4" id="chg-pwd-txt">Terms and Conditions</h4>
        <img src="{{asset('images/user-1.png')}}" alt="" class="edit-pro-png">
    </div>
</div>
<div class="t-c-body">
    <div class="set-edt-btn" id="t-c-btns">
        <button type="submit" class="btn  btn-lg set-save-btn">ACCEPT</button>
        <button type="submit" class="btn  btn-lg set-cancel-btn">DECLINE</button>
    </div>
</div>
