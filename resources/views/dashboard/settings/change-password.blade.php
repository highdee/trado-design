<div class="edit-header-wrapper">
    <div class="tab-arrow-back">
        <img src="{{asset('images/arrow_simple_chock-34.png')}}" alt="" class="tab-arrow-backImg d-md-none">
    </div>
    <div class="edit-header" id="chg-pwd-header">
        <h4 class="settings-h4" id="chg-pwd-txt">Change Password</h4>
        <img src="{{asset('images/user-1.png')}}" alt="" class="edit-pro-png">
    </div>
</div>
<div class="settings-chgPwd-wrapper">
    <form action="">
        <div class="form-group ">
            <label for="old-password">Old Password</label>
            <input type="text" class="form-control" placeholder="">
            <img src="{{asset('images/eye-off-6.png')}}" alt="" class="chg-pwd-img">
        </div>
        <div class="form-group ">
            <label for="new-password">New Password</label>
            <input type="text" class="form-control" placeholder="at least 8 characters for security reasons">
            <img src="{{asset('images/eye-off-6.png')}}" alt="" class="chg-pwd-img">
        </div>
        <div class="form-group ">
            <label for="new-password">New Password</label>
            <input type="text" class="form-control" placeholder="at least 8 characters for security reasons ">
            <img src="{{asset('images/eye-off-6.png')}}" alt="" class="chg-pwd-img">
        </div>

        <div class="set-pwd-btn">
            <button type="submit" class="btn  btn-lg set-save-btn">SAVE</button>
            <button type="submit" class="btn  btn-lg set-cancel-btn">CANCEL</button>
        </div>
    </form>
</div>
