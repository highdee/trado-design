@extends('layouts.dashboard')

@section('content')
    <main>
        <div class="settings-nav-wrapper">
            <div class="settings-nav-container">
                    <div class="settings-header-container">
                        <h4 class="settings-h4">Settings</h4>
                        <img src="{{asset('images/settings-19.png')}}" alt="" class="edit-pro-png">
                    </div>
                <div class="settings-nav">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-link active sett-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                                <img src="{{asset('images/user-1.png')}}" alt="" class="set-nav-img1">
                                <img src="{{asset('images/user.png')}}" alt="" class="set-nav-img2">
                                <span>Edit Account</span>
                                <img src="{{asset('images/arrow_simple_chock-34.png')}}" alt="" class="tab-nav-arrow d-md-none">
                            </a>
                            <a class="nav-link sett-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                                <img src="{{asset('images/password-1.png')}}" alt="" class="set-nav-img1">
                                <img src="{{asset('images/password.png')}}" alt="" class="set-nav-img2">
                                <span>Change Password </span>
                                <img src="{{asset('images/arrow_simple_chock-34.png')}}" alt="" class="tab-nav-arrow d-md-none">
                            </a>
                            <a class="nav-link sett-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">
                                <img src="{{asset('images/insurance-3.png')}}" alt="" class="set-nav-img1">
                                <img src="{{asset('images/insurance.png')}}" alt="" class="set-nav-img2">
                                <span>Terms and Conditions</span>
                                <img src="{{asset('images/arrow_simple_chock-34.png')}}" alt="" class="tab-nav-arrow d-md-none">
                            </a>
                            <a class="nav-link sett-link" id="nav-privacyPolicy-tab " data-toggle="tab" href="#nav-privacyPolicy" role="tab" aria-controls="nav-privacyPolicy" aria-selected="false">
                                <img src="{{asset('images/accept-3.png')}}" alt="" class="set-nav-img1">
                                <img src="{{asset('images/accept.png')}}" alt="" class="set-nav-img2">
                                <span>Privacy Policy</span>
                                <img src="{{asset('images/arrow_simple_chock-34.png')}}" alt="" class="tab-nav-arrow d-md-none">
                            </a>
                            <div class="set-nav-logout">
                                <div class="set-logout-wrapper">
                                    <a href="#" class="set-logout-link active d-sm-block d-md-none">
                                        <img src="{{ asset('/images/XMLID.png') }}">
                                        <span>LogOut</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">@include('dashboard/settings/edit-profile')</div>
                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">@include('dashboard/settings/change-password')</div>
                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">@include('dashboard/settings/terms-conditions')</div>
                        <div class="tab-pane fade" id="nav-privacyPolicy" role="tabpanel" aria-labelledby="nav-privacyPolicy-tab">@include('dashboard/settings/privacy-policy')</div>
{{--                        <div class="tab-pane fade" id="nav-set" role="tabpanel" aria-labelledby="nav-set-tab">@include('dashboard/settings/set')</div>--}}
                    </div>

                </div>

            </div>
        </div>
    </main>
@endsection

@section('script')
    <script>
        $('document').ready(function(){
            if ($(window).width() < 776) {
                $('.sett-link ').click(function () {
                    $('.settings-nav nav').css('display', 'none');
                    $('.tab-content').css('display', 'block');
                    $('.settings-nav-container').css('background', 'transparent');
                    $('.settings-header-container').css('display', 'none')
                })
                $('.tab-arrow-backImg').click(function () {
                    $('.settings-nav nav').css('display', 'block');
                    $('.tab-content').css('display', 'none');
                    $('.settings-nav-container').css('background', 'green');
                    $('.settings-header-container').css('display', 'block')
                })
            }
        })
    </script>
@endsection
