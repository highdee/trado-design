<div class="edit-header-wrapper" id="chgPwd-header-wrapper">
    <div class="tab-arrow-back">
        <img src="{{asset('images/arrow_simple_chock-34.png')}}" alt="" class="tab-arrow-backImg d-md-none">
    </div>
    <div class="edit-header">
        <h4 class="settings-h4">Edit Account</h4>
        <img src="{{asset('images/user-1.png')}}" alt="" class="edit-pro-png">
    </div>
    <div class="settings-pro1-img">
        <img src="{{asset('images/asset-67.png')}}" alt="" class="set-pro-img">
        <a href="#" class="btn btn-default chng-pro-btn">Change Profile Picture</a>
    </div>
</div>
<div class="settings-profile-wrapper">
    <div class="settings-pro-img">
        <img src="{{asset('images/asset-67.png')}}" alt="" class="set-pro-img">
        <a href="#" class="btn btn-default chng-pro-btn">Change Profile Picture</a>
    </div>
</div>
<div class="settings-edtForm-wrapper">
    <form action="">
        <div class="form-row">
            <div class="form-group col-sm-12 col-md-6">
                <label for="first name">First Name</label>
                <input type="text" class="form-control" placeholder="Ogungbagbe">
            </div>
            <div class="form-group col-sm-12 col-md-6">
                <label for="last name">Last Name</label>
                <input type="text" class="form-control" placeholder="Oluwasegun ">
            </div>
            <div class="form-group col-sm-12 col-md-6">
                <label for="email">E-mail Address</label>
                <input type="email" class="form-control" placeholder="e.g tradofamily@gmail.con">
            </div>
            <div class="form-group set-phNumb-wrapper col-sm-12 col-md-6">
                <label for="phone number">Phone Number</label>
                <input type="text" class="form-control" id="set-phNumb-inp" placeholder="8144556677">
                <div class="phone-inp-img">
                    <img src="{{asset('images/nigeria-1.png')}}" alt="" class="frm-inp-img">
                    <img src="{{asset('images/arrow_simple_chock-20.png')}}" alt="" class="frm-inp-arrow">
                </div>
            </div>
        </div>
        <div class="set-edt-btn">
            <button type="submit" class="btn hoverable  btn-lg set-save-btn">SAVE</button>
            <button type="submit" class="btn hoverable  btn-lg set-cancel-btn">CANCEL</button>
        </div>
    </form>
</div>
