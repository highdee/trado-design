@extends('layouts.dashboard')

@section('content')
    <main>
        <div class="savings-header">
            <div class="savings-txt-wrapper">
                <p class="savings-p"><span class="saving-dash">Dashboard</span>>Savings</p>
            </div>
            <div>
                <img src="{{asset('images/asset-131.png')}}" class="savings-img" alt="">
            </div>
        </div>
        <!-- Available balance -->
        <div class="savings-balance-wrapper">
            <div class="savings-bal-inner">
                <p class="savings-p-head">Available balance</p>
                <div class="savings-balance">
                    <h4>
                        <span class="savings-amt">45,000</span>
                        <span class="savings-n">N</span>
                    </h4>
                </div>
                <div class="savings-event-wrapper">
                    <div class="s-event-info" id="sav1-box" onclick="openModal('modal-wrapper')">
                        <h6 class="event-amt">N100,000 <span class="fa fa-angle-down"></span> </h6>
                        <p class="s-event-p">For Upcoming Events</p>
                    </div>
                    <div class="s-event-info" id="sav2-box" onclick="openModal('modal-wrapper-withdrawal')">
                        <h6 class="event-amt">N255,000 <span class="fa fa-angle-down"></span> </h6>
                        <p class="s-event-p">For Upcoming Investment</p>
                    </div>
                </div>
                <div class="savings-event-footer">
                    <div class="saving-modal-wrapper">
                        <a href="javascript:void(0)" class="btn save-up-btn1" onclick="addDom('#save_up_modal', 'show')">
                            <img src="{{asset('images/savings-29.png')}}" alt="" class="save-up-btnImg1">
                            <span class="save-up-s">SAVE UP</span>
                        </a>
                        <!--  Page modal -->
                        <div class="upcoming-popUp-wrapper" id="modal-wrapper">
                            <div class="upcoming-popUp-box">
                                <div class="up-popUp-header">
                                    <div class="up-popUp-title">
                                        <h3 class="up-event-numb">N100,000</h3>
                                        <p>For Upcoming Events</p>
                                    </div>
                                    <div class="up-popUp-close" id="closeModal" onclick="closeModal('modal-wrapper')">
                                        <img src="{{asset('images/mini_x_-4.png')}}" alt="">
                                    </div>
                                </div>
                                <div class="up-popUp-table">
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th scope="col">Title</th>
                                                <th scope="col">Amount</th>
                                                <th scope="col">Date</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1" onclick="widthdrawal()">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1" onclick="widthdrawal()">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1" onclick="widthdrawal()">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1" onclick="widthdrawal()">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1" onclick="widthdrawal()">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1" onclick="widthdrawal()">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="upcoming-popUp-wrapper" id="modal-wrapper-withdrawal">
                            <div class="upcoming-popUp-box">
                                <div class="up-popUp-header">
                                    <div class="up-popUp-title">
                                        <h3 class="up-event-numb">N255,000</h3>
                                        <p>For Upcoming Investments</p>
                                    </div>
                                    <div class="up-popUp-close" id="closeModal" onclick="closeModal('modal-wrapper-withdrawal')">
                                        <img src="{{asset('images/mini_x_-4.png')}}" alt="">
                                    </div>
                                </div>
                                <div class="up-popUp-table">
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th scope="col">Title</th>
                                                <th scope="col">Amount</th>
                                                <th scope="col">Date</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1 ">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1 ">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1 ">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1 ">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1 ">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popUp-title">Friend's Event</td>
                                                <td class="popUp-amt">N30,000</td>
                                                <td class="popUp-date">Dec 10,2020</td>
                                                <td>
                                                    <a href="#"class="btn pop-up-btn1 ">
                                                        <img src="{{asset('images/savings-29.png')}}" alt="" class="pop-up-btnImg">
                                                        <span class="pop-up-s">WITHDRAW</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="pagemodal" data-name="page-modal" class="modal">
                            <div id="save_up_modal" data-active="false" class="top-up-alert save_up">
                                <div class="trd-notification-top">
                                    <h5 style="color: #000" class="save_up-head_text">
                                        Save Up
                                    </h5>
                                    <button type="button" onclick="removeDom('#save_up_modal', 'show')">
                                        <img src="{{ asset('/images/mini_x.png') }}" alt="_trado-projects">
                                    </button>
                                </div>
                                <!-- top up-->
                                <div class="ttrd-top-alert">
                                    <!-- Tab links -->
                                    <div class="tab open">
                                        <button class="tablinks save_up_tab">
                                            <!--Get the account info from the button-->
                                            <div class="tabs-payment">
                                                <h4 class="save-up-tab-header">Save to E-wallet</h4>
                                                <p>
                                                    <span>
                                                        This will be saved directly to your Available balance and can be Withdrawn
                                                        easily anytime you want it
                                                    </span>
                                                </p>
                                            </div>
                                        </button>
                                        <button class="tablinks save_up_tab disabled mb-0">
                                            <div class="tabs-payment mb-0">
                                                <h4 class="save-up-tab-header">Save for Urgencies</h4>
                                                <p class="mb-0">
                                                    <span>
                                                        This will be locked till your expected date and then be made available
                                                        to you only after the fixed period We want your happiness
                                                    </span>
                                                </p>
                                            </div>
                                        </button>
                                    </div>
                                    <!-- Tab content -->
                                    <div id="virtual_payment" class="tabcontent">
                                    </div>
                                    <div id="atm_payment" class="tabcontent save_up">
                                        <form>
                                            <div class="form-group disabled">
                                                <label for="Title">Title</label>
                                                <input type="text" class="form-control" placeholder="please select an option">
                                            </div>
                                            <div class="form-group disabled">
                                                <label for="duration">Duration</label>
                                                <input type="text" class="form-control" placeholder="number of weeks of exact Date(4 Months or December 24)">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab open mt-3">
                                        <button class="tablinks save_up_tab mb-0 disabled">
                                            <div class="tabs-payment mb-0">
                                                <h4 class="save-up-tab-header">Save for Investment</h4>
                                                <p class="mb-0">
                                                    <span>
                                                        You can now save up bit by bit for the Upcoming Investment such that you have
                                                        enough to invest to as many Plantations as you want. It will be locked till
                                                        when Investment is Available
                                                    </span>
                                                </p>
                                            </div>
                                        </button>
                                    </div>
                                    <button type="button" form="active_form" class="btn btn-top-up save-up-submit">PAY NOW</button>
                                </div>
                            </div>
                            <!-- WITHDRAWAL MODAL -->
                            <div id="widthdrwal_modal" data-active="false" class="top-up-alert save_up wiithdrawal">
                                <div class="trd-notification-top">
                                    <h5 style="color: #000" class="save_up-head_text">
                                        Withdraw
                                    </h5>
                                    <button type="button" id="close-top-up" onclick="removeDom('#widthdrwal_modal', 'show')">
                                        <img src="{{ asset('/images/mini_x.png') }}" alt="_trado-projects">
                                    </button>
                                </div>
                                <!-- top up-->
                                <div class="ttrd-top-alert">
                                    <!-- Tab links -->
                                    <div class="tab open">
                                        <button class="tablinks save_up_tab" onclick="openpayment(event, 'virtual_payment')">
                                            <!--Get the account info from the button-->
                                            <div class="tabs-payment withdrawal">
                                                <h4 class="save-up-tab-header">Withdraw from E-wallet</h4>
                                                <p>
                                                    <span>
                                                        Withdraw from the Available Savings in your E-wallet
                                                    </span>
                                                </p>
                                            </div>
                                        </button>
                                        <button class="tablinks save_up_tab disabled mb-0" onclick="openCity(event, 'atm_payment')">
                                            <div class="tabs-payment withdrawal mb-0">
                                                <h4 class="save-up-tab-header">Widthdraw from Urgencies</h4>
                                                <p class="mb-0">
                                                    <span>
                                                        You can continue saving up this. Don't forget the target.
                                                        you can break this up if urgently needed
                                                    </span>
                                                </p>
                                            </div>
                                        </button>
                                    </div>
                                    <!-- Tab content -->
                                    <div id="virtual_payment" class="tabcontent">
                                    </div>
                                    <div id="atm_payment" class="tabcontent save_up">
                                        <form>
                                            <div class="form-group disabled">
                                                <label for="Title">Title</label>
                                                <input type="text" class="form-control" placeholder="please select an option">
                                            </div>
                                            <div class="form-group disabled">
                                                <label for="duration">Duration</label>
                                                <input type="text" class="form-control" placeholder="number of weeks of exact Date(4 Months or December 24)">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab open mt-3">
                                        <button class="tablinks save_up_tab mb-0 disabled"  onclick="openCity(event, 'atm_payment')">
                                            <div class="tabs-payment withdrawal mb-0">
                                                <h4 class="save-up-tab-header">Break Investment SaveUp</h4>
                                                <p class="mb-0">
                                                    <span>
                                                        You can now save up bit by bit for the Upcoming Investment such that you have
                                                        enough to invest to as many Plantations as you want. It will be locked till
                                                        when Investment is Available
                                                    </span>
                                                </p>
                                            </div>
                                        </button>
                                    </div>
                                    <button type="button" form="active_form" class="btn btn-top-up save-up-submit">WITHDRAW</button>
                                </div>
                            </div>
                            <!-- TRANSACTION SUCCESSFUL -->
                            <div id="transaction_alert" data-active="false" class="transaction_succesful">
                                <div class="trd-referral-top">
                                    <button type="button" id="close-notification" onclick="removeDom('#transaction_alert', 'show')">
                                        <img src="{{ asset('/images/mini_x.png') }}" alt="_trado-projects">
                                    </button>
                                    <div class="clear-fix"></div>
                                </div>
                                <div class="inner">
                                    <div class="inner-wrapper">
                                        <h5>Transaction Successful</h5>
                                        <img src="{{ asset("/images/Success_Illus.svg") }}">
                                        <a href="/user/dashboard" class="btn btn-grp-2">RETURN TO DASHBOARD</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Of Page modal -->
                    </div>
                    <a href="javascript:void(0)" onclick="addDom('#widthdrwal_modal', 'show')" class="btn save-up-btn2" >
                        <img src="{{asset('images/savings-29.png')}}" class="save-up-btnImg1">
                        <span class="save-up-s">WITHDRAW</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="transactions-wrapper">
            <div class="trans-box">
                <p class="trans-p">Transactions</p>
                <p class="trans-see-all">See All</p>
            </div>
            <div class="transaction-table-wrapper">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td>
                                <div class="table-title-wrapper">
                                    <div class="trans-table-img">
                                        <img src="{{asset('images/savings-58.png')}}" alt="">
                                    </div>
                                    <div class="trans-table-title">
                                        <p class="trans-title">Savings</p>
                                        <p class="trans-sub-title">Core Savings</p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <p class="trans-table-description">( Reference ID )</p>
                            </td>
                            <td>
                                <p class="trans-table-amt">N5,000</p>
                                <p class="trans-table-date">Aug. 5,2020</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="table-title-wrapper">
                                    <div class="trans-table-img">
                                        <img src="{{asset('images/savings-58.png')}}" alt="">
                                    </div>
                                    <div class="trans-table-title">
                                        <p class="trans-title">Savings</p>
                                        <p class="trans-sub-title">Core Savings</p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <p class="trans-table-description">( Reference ID )</p>
                            </td>
                            <td>
                                <p class="trans-table-amt">N5,000</p>
                                <p class="trans-table-date">Aug. 5,2020</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="table-title-wrapper">
                                    <div class="trans-table-img">
                                        <img src="{{asset('images/savings-58.png')}}" alt="">
                                    </div>
                                    <div class="trans-table-title">
                                        <p class="trans-title">Savings</p>
                                        <p class="trans-sub-title">Core Savings</p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <p class="trans-table-description">( Reference ID )</p>
                            </td>
                            <td>
                                <p class="trans-table-amt">N5,000</p>
                                <p class="trans-table-date">Aug. 5,2020</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </main>
@endsection

@section('script')
    <script>
        function openModal(modalID){
            document.getElementById(modalID).style.display = 'flex';
        };
        
        function closeModal(modalID){
            document.getElementById(modalID).style.display = 'none';
        };

        window.onclick = function(e) {
            var modal1 = document.getElementById('modal-wrapper')
            var modal2 = document.getElementById('modal-wrapper-withdrawal')

            if (e.target == modal1) {
                modal1.style.display = "none";
            }

             if (e.target == modal2) {
                modal2.style.display = "none";
            }
        };

        function widthdrawal(){
            closeModal('modal-wrapper')
            closeModal('modal-wrapper-withdrawal')
            addDom('#transaction_alert', 'show')
        };
    </script>
@endsection
