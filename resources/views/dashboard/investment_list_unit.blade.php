@extends('layouts.dashboard')

@section('content')
    <main>
        <!-- MOALS -->
        <div class="modal  data-name="page-modal" id="pagemodal" style="background: #032104bf">
             <!-- PLANTATION  SALES-->
            <div id="plantation_sales" data-active="false" class="purchase-alert sales unit-buy">
                <div class="trd-purchase-body pt-4">
                    <div class="trd-pruchase-title">
                        <h5>Buy Investments Units</h5>
                    </div>
                    <div class="trd-body-content">
                        <div class="trd-body-top plantation-chart">
                            <div class="trd-srd-2 roi-extended">
                                <b class="roi-new-text">ROI:</b> 25%
                            </div>
                            <div class="trd-srd-1">
                                <div id="plantation_chart" class="circle-progress">
                                    <span class="percentage type-top" data-value="60"></span>
                                </div>
                                <div class="wrapper-unit">
                                    <div class="name">
                                        <span>Name:</span>
                                        Pineapple  Plantation
                                    </div>
                                    <div>
                                        <span>
                                            <img src="{{asset('/images/wall-clock.png')}}">
                                            Time remaining:
                                        </span>
                                        <em>2</em>mon. 
                                        <em>10</em>days.
                                    </div>
                                    <div class="chart-timing">
                                        <span>
                                            <img src="{{asset('/images/money-bag.png')}}"> N120,000
                                        </span>
                                        <span class="ml-3">8 units </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form>
                            <div class="form-group">
                                <label>Number of units</label>
                                <input type="text" class="form-control border-buy-extt" placeholder="Input the number of Units to buy">
                            </div>
                            <div class="form-group">
                                <p>
                                    <span> Estimated Amount based on unit: </span>
                                    <span class="roi-points">N75,000</span>
                                </p>
                            </div>
                            <div class="form-group">
                                <button type="button" style="display: inherit" class="btn btn-sb-buy mt-5">SELL UNITS</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="trd-invs-wrapper bg-unit-primary">
            <div class="trd-invs-top bg-unit-primary">
                <div class="farmshop-header investment bg-unit-primary mb-0">
                    <div class="farmhead-txt-wrapper">
                        <h6 class="unit-sales-text">Unit Sales</h6>
                    </div>
                    <div>
                        <img src="{{asset('images/asset-131.png')}}" class="farmhead-img" alt="">
                    </div>
                </div>
                <div class="topinvestments unit-sales">
                    <div class="inv-chart unit-sales">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="chart-box-wrapper">
                                    <div class="chart-box">
                                        <div class="chart-box-top">
                                            <div id="circle_circle_1" class="circle-progress">
                                                <span class="percentage" data-value="60"></span>
                                            </div>
                                            <div class="wrapper-unit">
                                                <div class="name">
                                                    <span>Name:</span>
                                                    Pineapple  Plantation
                                                </div>
                                                <div>
                                                    <span>
                                                        <img src="{{asset('/images/wall-clock.png')}}">
                                                        Time remaining:
                                                    </span>
                                                    <em>2</em>mon. 
                                                    <em>10</em>days.
                                                </div>
                                                <div class="chart-timing">
                                                    <span>
                                                        <img src="{{asset('/images/money-bag.png')}}"> N120,000
                                                    </span>
                                                    <span>Units: 4</span>
                                                    <span>ROI: <em>25%</em></span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="buy_units_btn">Buy units</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="chart-box-wrapper">
                                    <div class="chart-box">
                                        <div class="chart-box-top">
                                            <div id="circle_circle_2" class="circle-progress">
                                                <span class="percentage" data-value="47"></span>
                                            </div>
                                            <div class="wrapper-unit">
                                                <div class="name">
                                                    <span>Name:</span>
                                                    Pineapple  Plantation
                                                </div>
                                                <div>
                                                    <span>
                                                        <img src="{{asset('/images/wall-clock.png')}}">
                                                        Time remaining:
                                                    </span>
                                                    <em>2</em>mon. 
                                                    <em>10</em>days.
                                                </div>
                                                <div class="chart-timing">
                                                    <span>
                                                        <img src="{{asset('/images/money-bag.png')}}"> N120,000
                                                    </span>
                                                    <span>Units: 4</span>
                                                    <span>ROI: <em>25%</em></span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="buy_units_btn">Buy units</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="chart-box-wrapper">
                                    <div class="chart-box">
                                        <div class="chart-box-top">
                                            <div id="circle_circle_3" class="circle-progress">
                                                <span class="percentage" data-value="57"></span>
                                            </div>
                                            <div class="wrapper-unit">
                                                <div class="name">
                                                    <span>Name:</span>
                                                    Pineapple  Plantation
                                                </div>
                                                <div>
                                                    <span>
                                                        <img src="{{asset('/images/wall-clock.png')}}">
                                                        Time remaining:
                                                    </span>
                                                    <em>2</em>mon. 
                                                    <em>10</em>days.
                                                </div>
                                                <div class="chart-timing">
                                                    <span>
                                                        <img src="{{asset('/images/money-bag.png')}}"> N120,000
                                                    </span>
                                                    <span>Units: 4</span>
                                                    <span>ROI: <em>25%</em></span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="buy_units_btn">Buy units</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="chart-box-wrapper">
                                    <div class="chart-box">
                                        <div class="chart-box-top">
                                            <div id="circle_circle_4" class="circle-progress">
                                                <span class="percentage" data-value="81"></span>
                                            </div>
                                            <div class="wrapper-unit">
                                                <div class="name">
                                                    <span>Name:</span>
                                                    Pineapple  Plantation
                                                </div>
                                                <div>
                                                    <span>
                                                        <img src="{{asset('/images/wall-clock.png')}}">
                                                        Time remaining:
                                                    </span>
                                                    <em>2</em>mon. 
                                                    <em>10</em>days.
                                                </div>
                                                <div class="chart-timing">
                                                    <span>
                                                        <img src="{{asset('/images/money-bag.png')}}"> N120,000
                                                    </span>
                                                    <span>Units: 4</span>
                                                    <span>ROI: <em>25%</em></span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="buy_units_btn">Buy units</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="chart-box-wrapper">
                                    <div class="chart-box">
                                        <div class="chart-box-top">
                                            <div id="circle_circle_5" class="circle-progress">
                                                <span class="percentage" data-value="30"></span>
                                            </div>
                                            <div class="wrapper-unit">
                                                <div class="name">
                                                    <span>Name:</span>
                                                    Pineapple  Plantation
                                                </div>
                                                <div>
                                                    <span>
                                                        <img src="{{asset('/images/wall-clock.png')}}">
                                                        Time remaining:
                                                    </span>
                                                    <em>2</em>mon. 
                                                    <em>10</em>days.
                                                </div>
                                                <div class="chart-timing">
                                                    <span>
                                                        <img src="{{asset('/images/money-bag.png')}}"> N120,000
                                                    </span>
                                                    <span>Units: 4</span>
                                                    <span>ROI: <em>25%</em></span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="buy_units_btn">Buy units</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="chart-box-wrapper">
                                    <div class="chart-box">
                                        <div class="chart-box-top">
                                            <div id="circle_circle_6" class="circle-progress">
                                                <span class="percentage" data-value="77"></span>
                                            </div>
                                            <div class="wrapper-unit">
                                                <div class="name">
                                                    <span>Name:</span>
                                                    Pineapple  Plantation
                                                </div>
                                                <div>
                                                    <span>
                                                        <img src="{{asset('/images/wall-clock.png')}}">
                                                        Time remaining:
                                                    </span>
                                                    <em>2</em>mon. 
                                                    <em>10</em>days.
                                                </div>
                                                <div class="chart-timing">
                                                    <span>
                                                        <img src="{{asset('/images/money-bag.png')}}"> N120,000
                                                    </span>
                                                    <span>Units: 4</span>
                                                    <span>ROI: <em>25%</em></span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="buy_units_btn">Buy units</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="chart-box-wrapper">
                                    <div class="chart-box">
                                        <div class="chart-box-top">
                                            <div id="circle_circle_7" class="circle-progress">
                                                <span class="percentage" data-value="82"></span>
                                            </div>
                                            <div class="wrapper-unit">
                                                <div class="name">
                                                    <span>Name:</span>
                                                    Pineapple  Plantation
                                                </div>
                                                <div>
                                                    <span>
                                                        <img src="{{asset('/images/wall-clock.png')}}">
                                                        Time remaining:
                                                    </span>
                                                    <em>2</em>mon. 
                                                    <em>10</em>days.
                                                </div>
                                                <div class="chart-timing">
                                                    <span>
                                                        <img src="{{asset('/images/money-bag.png')}}"> N120,000
                                                    </span>
                                                    <span>Units: 4</span>
                                                    <span>ROI: <em>25%</em></span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="buy_units_btn">Buy units</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="chart-box-wrapper">
                                    <div class="chart-box">
                                        <div class="chart-box-top">
                                            <div id="circle_circle_8" class="circle-progress">
                                                <span class="percentage" data-value="58"></span>
                                            </div>
                                            <div class="wrapper-unit">
                                                <div class="name">
                                                    <span>Name:</span>
                                                    Pineapple  Plantation
                                                </div>
                                                <div>
                                                    <span>
                                                        <img src="{{asset('/images/wall-clock.png')}}">
                                                        Time remaining:
                                                    </span>
                                                    <em>2</em>mon. 
                                                    <em>10</em>days.
                                                </div>
                                                <div class="chart-timing">
                                                    <span>
                                                        <img src="{{asset('/images/money-bag.png')}}"> N120,000
                                                    </span>
                                                    <span>Units: 4</span>
                                                    <span>ROI: <em>25%</em></span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="buy_units_btn">Buy units</a>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <span class="bg-trand"></span>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('script')
    <script src="{{ asset('/js/circle-progress.js') }}"></script>
    <script>
        var c1 = $('#circle_circle_1');
        var c2 = $('#circle_circle_2');
        var c3 = $('#circle_circle_3');
        var c4 = $('#circle_circle_4');
        var c5 = $('#circle_circle_5');
        var c6 = $('#circle_circle_6');
        var c7 = $('#circle_circle_7');
        var c_p = $('#plantation_chart');
        var c8 = $('#circle_circle_8');

        function drawChart(box, empty = true){
            var chartVal = box.find(".percentage").attr("data-value");
            box.children().text(`${chartVal}%`)

            if($(document).width() >= 1200){
                box.circleProgress({
                    startAngle: -Math.PI / 4 * 2,
                    value: chartVal / 100,
                    size: 140,
                    thickness: 22,
                    emptyFill: "#ffbe74",
                    lineCap: 'round',
                    fill: {color: '#06fd97'}
                });
            }else{
                box.circleProgress({
                    startAngle: -Math.PI / 4 * 2,
                    value: chartVal / 100,
                    size: 100,
                    thickness: 17,
                    emptyFill: "#ffbe74",
                    lineCap: 'round',
                    fill: {color: '#06fd97'}
                });
            }
        }        

        drawChart(c1)
        drawChart(c2)
        drawChart(c3)
        drawChart(c4)
        drawChart(c5)
        drawChart(c6)
        drawChart(c7)
        drawChart(c8)
        drawChart(c_p)
        
    </script>
    <script>
       
    </script>
@endsection
