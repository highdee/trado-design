@extends('layouts.dashboard')

@section('header-link')
    <link href="{{ asset('css/project-update.css') }}" rel="stylesheet">
@endsection

@section('content')
    <main>
        <div class="project-wrapper">
            <div class="trd-dss-wrapper">
                <div class="container-fluid trds-pst">
                    <div class="trd-dss-204" id="project-header-wrapper">
                        <div class="project-img-wrap">
                            <img src="{{asset('/images/trado-project-update.png')}}" alt="">
                            <h4 class="project-h4">Project Updates</h4>
                        </div>
                        <div class="img-userImg-wrap">
                            <img src="{{asset('images/asset-131.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="project-body-wrapper">
                <div class="row">
                    <div class="col-sm-12 col-md-6 mb-3">
                        <div class="project-wrapper-project">
                            <p class="project-pTxt">
                                The Plantation process Video
                            </p>
                            <div class="prj-video">
                                <video id="plantation-vid">
                                    <source src="{{asset('images/video.mp4')}}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                                <button id="playBtn" onclick="playVideo('#plantation-vid')">
                                    <img src="{{ asset('/images/play-button.png') }}">
                                </button>
                            </div>
                            <p class="project-foot-txt"><span class="project-colored-txt">Posted</span> :Tuesday 5th of October,2020 | 4p.m</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 mb-3">
                        <div class="project-wrapper-project img-wrapper">
                            <p class="project-pTxt">
                                The Plantation process Video
                            </p>
                            <div class="prj-img">
                                <img src="{{asset('/images/EjChcNoXkAAX4Sx.png')}}">
                            </div>
                            <p class="project-foot-txt"><span class="project-colored-txt">Posted</span> :Tuesday 5th of October,2020 | 4p.m</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 mb-3">
                        <div class="project-wrapper-project img-wrapper">
                            <p class="project-pTxt">
                                The Plantation process Video
                            </p>
                            <div class="prj-img">
                                <img src="{{asset('/images/iauskdfj_reformed.png')}}">
                            </div>
                            <p class="project-foot-txt">
                                <span class="project-colored-txt">Posted</span> :Tuesday 5th of October,2020 | 4p.m
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 mb-3">
                        <div class="project-wrapper-project">
                            <p class="project-pTxt">
                                Few Days Plantation
                            </p>
                            <div class="prj-video">
                                <video id="days-plantation">
                                    <source src="{{asset('/images/video.mp4')}}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                                <button id="playBtn" onclick="playVideo('#days-plantation')">
                                    <img src="{{ asset('/images/play-button.png') }}">
                                </button>
                            </div>
                            <p class="project-foot-txt"><span class="project-colored-txt">Posted</span> :Tuesday 5th of October,2020 | 4p.m</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection

@section('script')
    <script>
        function playVideo(videoID){
            document.querySelector(videoID).play()
        }

        function pauseVideo(videoID){
            document.querySelector(videoID).pause()
        }
    </script>
@endsection
