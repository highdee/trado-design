@extends('layouts.dashboard')

@section('content')
    <main>
        <div class="trd-dash">
            <section class="trd-dss-top">
                <!-- DASHBORD TOP -->
                <div class="trd-dss-wrapper">
                    <div class="container-fluid trds-pst">
                        <div class="trd-dss-204">
                            <div class="trd-dss-304">
                                <h4>Welcome <span class="trd-dss-name">Segun</span></h4>
                                <p>
                                    <span>Ensure you keep and stay safe!</span>
                                    <img src="{{ asset('/images/medical_mask.svg') }}">
                                </p>
                            </div>
                            <div class="trd-dss-404">
                                <div class="trd-dss-444">
                                    <a href="/user/dashboard/project-update">
                                        <img src="{{ asset('/images/trado-project-update.png') }}" alt="_trado-projects">
                                    </a>
                                </div>
                                <div class="trd-dss-444 trd-dssh-437 mx-lg-4">
                                    <a href="javascript:void(0)" id="alert-notification" >
                                        <img src="{{ asset('/images/notification.svg') }}" alt="_trado-projects">
                                        <span data-alert="3">3</span>
                                    </a>
                                </div>
                                <div class="trd-dss-454">
                                    <button type="button" class="btn trd-btn-user">
                                        <a href="/user/dashboard/settings"> <img src="{{ asset('/images/user1.png') }}" alt="_user"></a>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trd-dss-full-wrap">
                    <!-- PAGE MODAL -->
                    <div id="pagemodal" data-name="page-modal" class="modal">
                        <!-- NOTIFICATION MODAL -->
                        <div id="notification" data-active="false" class="notification-alert">
                            <div class="trd-not-top">
                                <h5>
                                    <img src="{{ asset('/images/notification.svg') }}" alt="_trado-projects">
                                    <span>Notifications</span>
                                </h5>
                                <button type="button" id="close-notification">
                                    <img src="{{ asset('/images/mini_x.png') }}" alt="_trado-projects">
                                </button>
                            </div>
                            <!-- NOTIFICATIONs-->
                            <div class="trd-nttr-388">
                                <ul class="mb-0">
                                    <li class="trd-notification-dis">
                                        <div class="trd-side-1">
                                            <span class="trd-side-img">
                                                <img src="{{ asset('/images/habaneros.jpg') }}" alt="">
                                            </span>
                                            <span><h4>Habanero Plantation is now Active for Investments</h4></span>
                                        </div>
                                        <div class="link">
                                            <a href="/user/dashboard/farmshop" class="link-in trd-noti-link"><span>View FarmShop</span> &#12297;</a>
                                        </div>
                                    </li>
                                    <li class="trd-notification-dis">
                                        <div class="trd-side-1">
                                            <span class="pl-4"><h4>Interest of N100.00 has been added to your E-wallet for October Intrest</h4></span>
                                        </div>
                                        <div class="link">
                                            <a href="/user/dashboard/transaction" class="link-in trd-noti-link"><span>Check transaction</span> &#12297;</a>
                                        </div>
                                    </li>
                                    <li class="trd-notification-dis">
                                        <div class="trd-side-1">
                                            <span class="trd-side-img">
                                                 <img src="{{ asset('/images/mockup-cucumber.png') }}" alt="">
                                            </span>
                                            <span><h4>Cucumber Plantation investments has about 50 units left</h4></span>
                                        </div>
                                        <div class="link">
                                            <a href="/user/dashboard/farmshop" class="link-in trd-noti-link"><span>View FarmShop</span> &#12297;</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- TOP-UP MODAL -->
                        <div id="top-up" data-active="false" class="top-up-alert">
                            <div class="trd-notification-top">
                                <h5>
                                    <span>Top Up</span>
                                </h5>
                                <button type="button" id="close-top-up">
                                    <img src="{{ asset('/images/mini_x.png') }}" alt="_trado-projects">
                                </button>
                            </div>
                            <!-- top up-->
                            <div class="ttrd-top-alert">
                                <!-- Tab links -->
                                <div class="tab open">
                                    <button class="tablinks" onclick="openPayment(event, 'virtual_payment')">
                                        <!--Get the account info from the button-->
                                       <div class="tabs-payment">
                                            <h4>from virtual account</h4>
                                            <p>
                                                <span>account name:</span>
                                                <b id="account_name">Akingbade segun</b>
                                            </p>
                                            <p>
                                                <span>account number: </span>
                                                <b id="account_number">244567345</b>
                                            </p>
                                       </div>
                                    </button>
                                    <button class="tablinks disabled mb-0" onclick="openPayment(event, 'atm_payment')">
                                        <div class="tabs-payment">
                                            <h4>From bank card(ATM)</h4>
                                        </div>
                                    </button>
                                </div>
                                <!-- Tab content -->
                                <div id="virtual_payment" class="tabcontent">
                                </div>
                                <div id="atm_payment" class="tabcontent">
                                   <form>
                                        <div class="form-group disabled">
                                            <label for="bank_name">bank name</label>
                                            <input type="text" class="form-control" name="bank_name">
                                        </div>
                                        <div class="form-group disabled">
                                            <label for="aacount_number">account number</label>
                                            <input type="text" class="form-control" name="aacount_number">
                                        </div>
                                        <div class="form-group disabled">
                                            <label for="card_number">card number</label>
                                            <input type="text" class="form-control" name="card_number">
                                        </div>
                                        <div class="form-group">
                                            <label for="amount">amount</label>
                                            <input type="text" class="form-control" name="amount">
                                        </div>
                                        <button type="button" class="btn btn-top-up">PAY NOW</button>
                                   </form>
                                </div>
                            </div>
                        </div>
                        <!-- WIDTHDRAW MODAL -->
                        <div id="withdrawal" data-active="false" class="top-up-alert withdrawal">
                            <div class="trd-notification-top">
                                <h5>
                                    <span>Withdraw</span>
                                </h5>
                                <button type="button" id="close-widthdrawal">
                                    <img src="{{ asset('/images/mini_x.png') }}" alt="_trado-projects">
                                </button>
                            </div>
                            <!-- top up-->
                            <div class="ttrd-top-alert">
                                <!-- Tab content -->
                                <div id="atm_payment" class="tabcontent">
                                   <form>
                                        <div class="form-group">
                                            <label for="bank_name">bank name</label>
                                            <input type="text" class="form-control" name="bank_name">
                                        </div>
                                        <div class="form-group ">
                                            <label for="aacount_number">account number</label>
                                            <input type="text" class="form-control" name="aacount_number">
                                        </div>
                                        <div class="form-group">
                                            <label for="card_number">card number</label>
                                            <input type="text" class="form-control" name="card_number">
                                        </div>
                                        <div class="form-group">
                                            <label for="amount">amount</label>
                                            <input type="text" class="form-control" name="amount">
                                        </div>
                                        <button type="button" class="btn btn-top-up">Withdraw</button>
                                   </form>
                                </div>
                            </div>
                        </div>
                        <!-- BUY PLANTATION -->
                        <div id="purchase" data-active="false" class="purchase-alert">
                            <div class="trd-not-top">
                                <button type="button" id="close-notification" onclick="removeDom('#purchase', 'show')">
                                    <img src="{{ asset('/images/mini_x.png') }}" alt="_trado-projects">
                                </button>
                                <div class="clear-fix"></div>
                            </div>
                            <div class="trd-purchase-body">
                                <div class="trd-pruchase-title">
                                    <h5>Buy Plantation</h5>
                                </div>
                                <div class="trd-body-content">
                                    <div class="trd-body-top">
                                        <div class="trd-srd-1">
                                            <h4>
                                                Name: Mango Plantation
                                            </h4>
                                            <h5>
                                                <img src="{{asset('images/trd-clock.png')}}" alt="">
                                                <span>Duration: </span>
                                                <span> 4 months</span>
                                            </h5>
                                        </div>
                                        <div class="trd-srd-2">
                                            ROI: 25%
                                        </div>
                                    </div>
                                    <form>
                                        <span>
                                            Mango is a tropical Asian fruit tree called Mangifera indica,
                                            It is a pickled vegetable or fruit with a spicy stuffing; a vegetable or fruit
                                            which has been managed. A green bell pepper suitable for pickling
                                            It is a pickled vegetable or fruit with a spicy stuffing; a vegetable or fruit which
                                            has been managed.
                                        </span>
                                        <div class="form-group">
                                            <label>Number of units</label>
                                           <input type="text" class="form-control" placeholder="Input the number of Units to be Invested">
                                        </div>
                                        <div class="payment-method-select" style="width:100%; margin-bottom:1rem">
                                            <div class="form-group mb-0">
                                                <label>Credit with</label>
                                            </div>
                                            <select>
                                                <option value="0">Please select where it should be funded with</option>
                                                <option value="option-1">Option-1</option>
                                                <option value="Option-2">Option-2</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <p>
                                                <span> Estimated Amount: </span>
                                                <span class="roi-points">N250,000</span>
                                            </p>
                                            <p>
                                                <span> Estimated Amount with Investment ROI : </span>
                                                <span class="roi-points">N250,000</span>
                                            </p>
                                            <p>
                                                <span> Estimated Payment Date :  </span>
                                                <span class="roi-points">FEBRUARY 15,2021</span>
                                            </p>
                                        </div>
                                        <div class="form-group form-modal-center">
                                            <button type="button" class="btn btn-sb-buy">BUY NOW</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- REFERRAL POINT -->
                        <div id="referral" data-active="false" class="referral-side">
                            <div class="trd-referral-top">
                                <button type="button" id="close-referral" style="margin-bottom: 3rem">
                                    <img src="{{ asset('/images/mini_x.png') }}" alt="_trado-projects">
                                </button>
                                <div class="clear-fix"></div>
                            </div>
                            <div class="trd-referral-body">
                                <div class="body-wrap">
                                    <h4>Earn per Referral</h4>
                                    <div class="trd-referral-content">
                                        <div class="trd-rffr-img">
                                            <img src="{{ asset('/images/Refer.png') }}">
                                        </div>
                                        <form>
                                            <div class="form-group">
                                                <label>Copy your Referral Link and ID</label>
                                                <input type="text" class="form-control" placeholder="The Referral link and ID here">
                                            </div>
                                            <button type="submit" class="btn btn-grp-2">COPY LINK/ID</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FEEDBACK MODAL -->
                        <div id="feedback_alert" data-active="false" class="transaction_succesful type-feed">
                            <div class="trd-referral-top">
                                <button type="button" id="close-notification">
                                    <img src="{{ asset('/images/mini_x.png') }}" alt="_trado-projects">
                                </button>
                                <div class="clear-fix"></div>
                            </div>
                            <div class="inner">
                                <div class="inner-wrapper type-2">
                                    <h5>Please, do give a feedback <br>
                                        about your Experience with Us
                                    </h5>
                                    <form>
                                        <div class="form-group">
                                            <textarea cols="30" class="form-control" rows="10">The message here</textarea>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-grp-2">SUBMIT</button>
                                            <a href="/user/dashboard" class="btn link-in">See What others are Saying <span>&#12297;</span></a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- FEEDBACK MODAL -->
                        <div id="feedback_alert" data-active="false" class="transaction_succesful testmonials">
                            <div class="trd-referral-top">
                                <button type="button" id="close-notification">
                                    <img src="{{ asset('/images/mini_x.png') }}" alt="_trado-projects">
                                </button>
                                <div class="clear-fix"></div>
                            </div>
                            <div class="inner">
                                <div class="inner-wrapper type-2 carousel">
                                    <h5>See What others <br>
                                        are Saying
                                    </h5>
                                    <div class="carousel-inner">
                                        <div class="owl-carousel">
                                            <div class="farm-card trd-frm-352 testionials-card">
                                                <div class="farm-sec1 xxlg">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- REFERRAL EARNING MODAL -->
{{--                    class="refferal-earning-modal"--}}
                    <div class="refferal-earning-modal">
                        <div id="refferal_earning" >
                            <div class="referral-earning-wrapper">
                                <div class="ern-ref-img-wrapp">
                                    <img src="{{asset('/images/mini_x_-3.png')}}" class="ern-ref-img" alt="">
                                </div>
                                <div class="ref_earning_amt">
                                    <h6 class="ref_earning">Referral Earning</h6>
                                    <div class="ref_ern_amt">
                                        <h1 class="">N8,000.00</h1>
                                    </div>
                                </div>
                                <div class="ref-ern_txt">
                                    <p class="ern-txt">
                                        Share with your colleagues and loved
                                        ones and get benefits as you share and
                                        they sign up with your Referral code/Link
                                    </p>
                                </div>
                                <div class="ref-ern-link-wrapper">
                                    <p class="ern-link-txt">Copy your Referral Link and ID</p>
                                    <textarea name="" class="ref-ern-input" id="" cols="30" rows="10" placeholder="The Referral link and ID here"> </textarea>
        {{--                                    <textarea type="text" class="ref-ern-input" placeholder="The Referral link and ID here">--}}
                                </div>
                                <div class="ref-ern-details">
                                    <div class="ern-details-wrapp">
                                        <div class="ern-ref-inner">
                                            <div class="ern-ref-icon">
                                                <i class="fa fa-user-plus ern-ref-i"></i>
                                            </div>
                                            <div class="ern-ref-bWrap">
                                                <div class="ern-referral-bonus">
                                                    <span class="ern-ref-b">Referral bonus</span>
                                                    <span class="ern-ref-name">-Ajayi Lawrence</span>
                                                </div>
                                                <div class="ref-date">November 5,2020</div>
                                            </div>
                                        </div>
                                        <div class="ern-ref-amount">N250</div>
                                    </div>
                                    <div class="ern-details-wrapp">
                                        <div class="ern-ref-inner">
                                            <div class="ern-ref-icon">
                                                <i class="fa fa-user-plus ern-ref-i"></i>
                                            </div>
                                            <div class="ern-ref-bWrap">
                                                <div class="ern-referral-bonus">
                                                    <span class="ern-ref-b">Referral bonus</span>
                                                    <span class="ern-ref-name">-Ajayi Lawrence</span>
                                                </div>
                                                <div class="ref-date">November 5,2020</div>
                                            </div>
                                        </div>
                                        <div class="ern-ref-amount">N250</div>
                                    </div>
                                    <div class="ern-details-wrapp">
                                        <div class="ern-ref-inner">
                                            <div class="ern-ref-icon">
                                                <i class="fa fa-user-plus ern-ref-i"></i>
                                            </div>
                                            <div class="ern-ref-bWrap">
                                                <div class="ern-referral-bonus">
                                                    <span class="ern-ref-b">Referral bonus</span>
                                                    <span class="ern-ref-name">-Ajayi Lawrence</span>
                                                </div>
                                                <div class="ref-date">November 5,2020</div>
                                            </div>
                                        </div>
                                        <div class="ern-ref-amount">N250</div>
                                    </div>
                                    <div class="ern-details-wrapp">
                                        <div class="ern-ref-inner">
                                            <div class="ern-ref-icon">
                                                <i class="fa fa-user-plus ern-ref-i"></i>
                                            </div>
                                            <div class="ern-ref-bWrap">
                                                <div class="ern-referral-bonus">
                                                    <span class="ern-ref-b">Referral bonus</span>
                                                    <span class="ern-ref-name">-Ajayi Lawrence</span>
                                                </div>
                                                <div class="ref-date">November 5,2020</div>
                                            </div>
                                        </div>
                                        <div class="ern-ref-amount">N250</div>
                                    </div>
                                </div>
                                <a href="#" class="btn referral-copy-link">COPY LINK/ID</a>
                            </div>
                        </div>
                    </div>


                    <div class="container-fluid">
                        <div class="row">
                            <!-- ACCOUNT DETAILS -->
                            <div class="col-xl-9">
                                <div class="trd-dsh-account">
                                    <div class="trd-dsh-inner">
                                        <div class="trd-dsh-scroll">
                                            <!-- CARD DETAILS BOX -->
                                            <div id="card-box-1" class="trd-dsh12-display trd-bdr trd-dsh-rounded trd-txt-light">
                                                <div class="spiral">
                                                    <div class="spiral-img">
                                                        <img src="{{asset('/images/asset-61.png')}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="details mb-2 mb-xl-2 w-100">
                                                        <div class="trd-card-info">
                                                            <span class="name-trd trd-llt">Akingbade Segun</span>
                                                            <p class="mb-0">1232 2342 3434 8349</p>
                                                        </div>
                                                        <div class="trd-comp">
                                                            <img src="{{ asset('/images/trado_white.png') }}">
                                                        </div>
                                                    </div>
                                                    <div class="trd-balance w-100">
                                                        <p class="trd-llt mb-0">Available balance</p>
                                                        <h4><sup>N</sup><span>45,000</span></h4>
                                                    </div>
                                                    <div class="trd-js-links w-100">
                                                        <a href="javascript:void(0)" id="top_up_btn">
                                                            <div class="img-wrapper">
                                                                <img src="{{ asset('/images/mini_plus.svg') }}">
                                                            </div>
                                                            <span class="no-wrap w3-font">Top Up</span>
                                                        </a>
                                                        <a href="javascript:void(0)" id="open-withdrawal-btn">
                                                            <div class="img-wrapper">
                                                                <img src="{{ asset('/images/withdraw.svg') }}">
                                                            </div>
                                                            <span class="w3-font">Withdraw</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- CARD SAVINGS BOX -->
                                            <a id="card-box-2" href="/user/dashboard/savings" class="trd-dsh12-display trd-jsd trd-bdr trd-dsh-rounded trd-txt-light">
                                                <div class="spiral">
                                                    <div class="spiral-img">
                                                        <img src="{{asset('/images/asset-63.png')}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="details mb-2 mb-xl-1 w-100">
                                                        <div class="trd-card-info">
                                                            <p class="cn-trd mb-0">Total Savings</p>
                                                        </div>
                                                    </div>
                                                    <div class="trd-balance w-100">
                                                        <h4><sup>N</sup><span>400,000</span></h4>
                                                    </div>
                                                    <div class="trd-js202-links w-100">
                                                        <div class="trd-js202-infos">
                                                            <h6>N145,000</h6>
                                                            <p class="mb-0">For Upcoming Events</p>
                                                        </div>
                                                        <div class="trd-js202-infos">
                                                            <h6>N225,000</h6>
                                                            <p class="mb-0">For Upcoming Investments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- CARD INVESTMENTS BOX -->
                                            <a id="card-box-3" href="/user/dashboard/transaction/investments" class="trd-dsh12-display trd-ins trd-bdr trd-dsh-rounded trd-txt-light">
                                                <div class="spiral">
                                                    <div class="spiral-img">
                                                        <img src="{{asset('/images/asset-62.png')}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="details mb-2 w-100">
                                                        <div class="trd-card-info">
                                                            <p class="cn-trd mb-0">Total Investments</p>
                                                        </div>
                                                    </div>
                                                    <div class="trd-balance w-100">
                                                        <h4><sup>N</sup><span>600,000</span></h4>
                                                    </div>
                                                    <div class="trd-js204-links w-100">
                                                        <div class="trd-progress-bar">
                                                            <!-- progress inserted into databute-progress attri -->
                                                            <div id="progress" class="trd-progress-val" data-progress="40"></div>
                                                            <span id="progress-value" class="trd-progess-ivl"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="box-nav">
                                            <button id="card-1" class="btn">
                                            </button>
                                            <button id="card-2" class="btn">
                                            </button>
                                            <button id="card-3" class="btn">
                                            </button>
                                        </div>
                                    </div>
                                    <div class="trd-dsh-activities">
                                        <div class="trd-act-wrapper">
                                            <div class="container-fluid px-0">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="trd-frm-435">
                                                            <div>
                                                                <h6>Recent Activities</h6>
                                                                <a href="/user/dashboard/transaction">
                                                                    See All
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="trd-act627-wrapper">
                                                <ul class="trd-activities-list">
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-1">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Savings</h5>
                                                                <p>Core Savings</p>
                                                            </span>
                                                        </div>
                                                        <div class="details type-x">
                                                            <h5 class="price">N5,000</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-1">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Withdrawal</h5>
                                                                <p>E-wallet</p>
                                                            </span>
                                                        </div>
                                                        <div class="details">
                                                            <h5 class="price">N5,000</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-2">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Investments</h5>
                                                                <p>5 Units of Pineapple Plantation</p>
                                                            </span>
                                                        </div>
                                                        <div class="details type-x">
                                                            <h5 class="price">N5,000</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-1">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Savings</h5>
                                                                <p>Core Savings</p>
                                                            </span>
                                                        </div>
                                                        <div class="details type-x">
                                                            <h5 class="price">N5,000</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-1">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Withdrawal</h5>
                                                                <p>E-wallet</p>
                                                            </span>
                                                        </div>
                                                        <div class="details">
                                                            <h5 class="price">N5,000</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-2">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Investments</h5>
                                                                <p>5 Units of Pineapple Plantation</p>
                                                            </span>
                                                        </div>
                                                        <div class="details type-x">
                                                            <h5 class="price">N5,000</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-1">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Savings</h5>
                                                                <p>Core Savings</p>
                                                            </span>
                                                        </div>
                                                        <div class="details type-x">
                                                            <h5 class="price">N5,000</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-1">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Withdrawal</h5>
                                                                <p>E-wallet</p>
                                                            </span>
                                                        </div>
                                                        <div class="details">
                                                            <h5 class="price">N5,000</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-2">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Investments</h5>
                                                                <p>5 Units of Pineapple Plantation</p>
                                                            </span>
                                                        </div>
                                                        <div class="details type-x">
                                                            <h5 class="price">N5,000</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ASIDE FARM SHOP -->
                            <div class="col-xl-3 px-xl-0">
                                <div class="trd-frmshop-inner">
                                    <div class="trd-farmshop-side">
                                        <div class="trd-frm-435 xxl">
                                            <div>
                                                <h6>FarmShop</h6>
                                                <a href="/user/dashboard/transaction ">
                                                    See All
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-xl-12">
                                                <div class="farm-card trd-frm-352">
                                                    <div class="farm-sec1 xxlg">
                                                        <img class="farm-img" src="{{ asset('images/cucumber_reform.png') }}" alt="">
                                                        <div class="farm-details">
                                                            <p class="plantation">Name of Plantation </p>
                                                            <h5 class="plant-name">Cucumber</h5>
                                                            <div class="plant-cost p-cost">
                                                                <div class="farm-i">
                                                                    <img src="{{ asset('/images/money.png') }}" alt="">
                                                                </div>
                                                                <div class="plant-amt">
                                                                    <p class="cycle">cost per unit</p>
                                                                    <p class="f-amt">N20,000</p>
                                                                </div>
                                                            </div>
                                                            <div class="plant-cost pc-mb-12">
                                                                <div class="farm-i img-dash">
                                                                    <img src="{{ asset('/images/time-42.png') }}" alt="" >
                                                                </div>
                                                                <div class="plant-amt">
                                                                    <p class="cycle">cycle duration</p>
                                                                    <p class="duration">6 Months</p>
                                                                </div>
                                                            </div>
                                                            <span class="roi">ROI:25%</span>
                                                        </div>
                                                    </div>
                                                    <div class="farm-sec2">
                                                        <div class="farm-location">
                                                            <div class="farm-i locate-i dashboard-location"><img src="{{ asset('/images/location_1.svg') }}"></div>
                                                            <p class="f-locate dashboard-lltr">Ajaokuta farm, Iwo, Osun State </p>
                                                        </div>
                                                        <a href="javascript:void(0)" class="farm-btn btn btn-lg btn-hover-primary" onclick="addDom('#purchase', 'show')">INVEST NOW</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-xl-12">
                                                <div class="farm-card trd-frm-352">
                                                    <div class="farm-sec1 xxlg">
                                                        <img class="farm-img" src="{{asset('images/miguel-andrade-nAOZCYcLND8-unsplash.jpg')}}" alt="">
                                                        <div class="farm-details">
                                                            <p class="plantation">Name of Plantation </p>
                                                            <h5 class="plant-name">Pineapple</h5>
                                                            <div class="plant-cost p-cost">
                                                                <div class="farm-i">
                                                                    <img src="{{ asset('/images/money.png') }}">
                                                                </div>
                                                                <div class="plant-amt">
                                                                    <p class="cycle">cost per unit</p>
                                                                    <p class="f-amt">N30,000</p>
                                                                </div>
                                                            </div>
                                                            <div class="plant-cost pc-mb-12">
                                                                <div class="farm-i img-dash">
                                                                    <img src="{{ asset('/images/time-42.png') }}">
                                                                </div>
                                                                <div class="plant-amt">
                                                                    <p class="cycle">cycle duration</p>
                                                                    <p class="duration">6 Months</p>
                                                                </div>
                                                            </div>
                                                            <span class="roi">ROI:35%</span>
                                                        </div>
                                                    </div>
                                                    <div class="farm-sec2">
                                                        <div class="farm-location">
                                                            <div class="farm-i locate-i dashboard-location"><img src="{{ asset('/images/location_1.svg') }}"></div>
                                                            <p class="f-locate dashboard-lltr">Ajaokuta farm, Iwo, Osun State </p>
                                                        </div>
                                                        <a href="javascript:void(0)" class="farm-btn btn btn-lg sold-out dash btn-trd btn-hover-primary" onclick="addDom('#purchase', 'show')">Invest now</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- REFER A FREIND -->
                                            <div class="trd-rrf-43r">
                                                <div class="container-fluid trd-xl-hidden">
                                                    <div class="row">
                                                        <div class="col"><h6 class="trd-rrf-top">Refer and Earn</h6></div>
                                                    </div>
                                                </div>
                                                <div class="trd-rrf-44r">
                                                    <div class="trd-rrf-wrap">
                                                        <div class="trd-rrf-inner xl">
                                                            <div class="container-fluid">
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <div class="trd-rrf-blog">
                                                                            <div class="trd-rrf-b-content">
                                                                                <div class="trd-rrf-b-img">
                                                                                    <img src="{{ asset('/images/AJIP.png') }}" alt="">
                                                                                </div>
                                                                                <div class="content-4243">
                                                                                    <h4 class="title">
                                                                                        Agribusiness<span>(Blog)</span>
                                                                                    </h4>
                                                                                    <p class="ctr-xxd">
                                                                                        Trado Global Limited harvests her first farm plantation as she  paid
                                                                                        out Okra and Pepper Investors <span>12,755,000</span> in Naira
                                                                                    </p>
                                                                                    <div class="link">
                                                                                        <a href="javascript:void(0)" id="refer_open" class="link-in float-right"><span>Learn more</span> &#12297;</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="container-fluid trd-sm-hidden">
                                                            <div class="row">
                                                                <div class="col"><h6 class="trd-rrf-top">Refer and Earn</h6></div>
                                                            </div>
                                                        </div>
                                                        <div class="trd-rrf-img">
                                                            <div class="container-fluid">
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <img src="{{ asset('/images/Refer.png') }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="trd-rrf-inner sm">
                                                            <div class="container-fluid">
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <div class="trd-rrf-blog">
                                                                            <div class="trd-rrf-b-content">
                                                                                <div class="trd-rrf-b-img">
                                                                                    <img src="{{ asset('/images/AJIP.png') }}" alt="">
                                                                                </div>
                                                                                <div class="content-4243">
                                                                                    <h4 class="title">
                                                                                        Agribusiness<span>(Blog)</span>
                                                                                    </h4>
                                                                                    <p class="ctr-xxd">
                                                                                        Trado Global Limited harvests her first farm plantation as she paid
                                                                                        out Okra and Pepper Investors <span>12,755,000</span> in Naira
                                                                                    </p>
                                                                                    <div class="link">
                                                                                        <a href="javascript:void(0)" id="refer_open2" class="link-in">Learn more <span>&#12297;</span> </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection

@section('script')
    <script>
        // styling
        function lsStyle(listItem){
            var lsItems = document.querySelectorAll(listItem)
            for(var i = 0; i < lsItems.length; i++){
                if( i & 1 != 0){
                    lsItems[i].style.backgroundColor = '#f5fff5'
                }
            }
        };
        lsStyle('.trd-activities-item');
        lsStyle('.trd-notification-dis');

        // WITDRAWAL FORM FUNCTION
        function openPayment(evt, checkedItem) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            document.getElementById(checkedItem).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // MODAL FUNCTION
        $('document').ready(function(){
            function alertModal(openbtn, closebtn, alert, modalClass, modal_true = true){
                $(openbtn).click(function(){
                    addDom(alert, modalClass, modal_true)
                });

                $(closebtn).click(function(){
                    removeDom(alert, modalClass, modal_true)
                });
            }

            function scrollCard(offsetPX){
                $(".trd-dsh-scroll").animate({scrollLeft: `${offsetPX}`}, 500)
            }

            (function(){
                alertModal('#alert-notification', '#close-notification', '#notification', 'show')
                alertModal('#top_up_btn', '#close-top-up', '#top-up', 'show')
                alertModal('#open-withdrawal-btn', '#close-widthdrawal', '#withdrawal', 'show')
                alertModal('#refer_open', '#close-referral', '#referral', 'show', false)
                alertModal('#refer_open2', '#close-referral', '#referral', 'show', false)
            })();

            (function progressBar(){
                var progress_val =  $('#progress').attr('data-progress')
                $('#progress').css('width', `${progress_val}%`)
                $('#progress-value').text(`${progress_val}%`)
            })();

            $('#card-1').click(function(){scrollCard(10)});
            $('#card-2').click(function(){scrollCard(410)});
            $('#card-3').click(function(){scrollCard(800)});

        });

        // CUSTOM SELECT FORM INPUT
        function dropSelect(){
            var x, i, j, l, ll, selElmnt, a, b, c;

            x = document.getElementsByClassName("payment-method-select");
            l = x.length;

            for (i = 0; i < l; i++) {
                selElmnt = x[i].getElementsByTagName("select")[0];
                ll = selElmnt.length;

                a = document.createElement("DIV");
                a.setAttribute("class", "select-selected");
                a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
                x[i].appendChild(a);

                b = document.createElement("DIV");
                b.setAttribute("class", "select-items select-hide");

                for (j = 1; j < ll; j++) {
                    /*for each option in the original select element,
                    create a new DIV that will act as an option item:*/
                    c = document.createElement("DIV");
                    c.innerHTML = selElmnt.options[j].innerHTML;
                    c.addEventListener("click", function(e) {
                        /*when an item is clicked, update the original select box,
                        and the selected item:*/
                        var y, i, k, s, h, sl, yl;
                        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                        sl = s.length;
                        h = this.parentNode.previousSibling;
                        for (i = 0; i < sl; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            yl = y.length;
                            for (k = 0; k < yl; k++) {
                            y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                        }
                        h.click();
                    });

                    b.appendChild(c);
                }

                x[i].appendChild(b);

                a.addEventListener("click", function(e) {
                    e.stopPropagation();
                    closeAllSelect(this);
                    this.nextSibling.classList.toggle("select-hide");
                    this.classList.toggle("select-arrow-active");
                });
            }

            function closeAllSelect(elmnt) {
                /*a function that will close all select boxes in the document,
                except the current select box:*/
                var x, y, i, xl, yl, arrNo = [];
                x = document.getElementsByClassName("select-items");
                y = document.getElementsByClassName("select-selected");
                xl = x.length;
                yl = y.length;
                for (i = 0; i < yl; i++) {
                    if (elmnt == y[i]) {
                        arrNo.push(i)
                    } else {
                        y[i].classList.remove("select-arrow-active");
                    }
                }

                for (i = 0; i < xl; i++) {
                    if (arrNo.indexOf(i)) {
                        x[i].classList.add("select-hide");
                    }
                }
            }

            document.addEventListener("click", closeAllSelect);
        }
        dropSelect()
    </script>
    <script src="{{ asset('js/circle-progress.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(".owl-carousel").owlCarousel();
        });
    </script>
@endsection
