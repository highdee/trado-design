@extends('layouts.dashboard')

@section('content')
    <main>
        <!-- MOALS -->
        <div class="modal" data-name="page-modal" id="pagemodal">
             <!-- PLANTATION  SALES-->
            <div id="plantation_sales" data-active="false" class="purchase-alert sales">
                <div class="trd-not-top">
                    <button type="button" id="close-notification" onclick="removeDom('#plantation_sales', 'show')">
                        <img src="{{ asset('/images/mini_x.png') }}" alt="_trado-projects">
                    </button>
                    <div class="clear-fix"></div>
                </div>
                <div class="trd-purchase-body">
                    <div class="trd-pruchase-title">
                        <h5>Plantation Sales</h5>
                    </div>
                    <div class="trd-body-content">
                        <div class="trd-body-top plantation-chart">
                            <div class="trd-srd-2">
                                ROI: 25%
                            </div>
                            <div class="trd-srd-1">
                                <div id="plantation_chart" class="circle-progress">
                                    <span class="percentage type-top" data-value="60"></span>
                                </div>
                                <div>
                                    <h4>
                                        Name: Mango Plantation
                                    </h4>
                                    <h5 class="mb-2">
                                        <img src="{{asset('images/trd-clock.png')}}" alt="">
                                        <span>Time remaining: </span>
                                        <span> <b>1</b> month <b>18</b> days</span>
                                    </h5>
                                    <h4>Total units: 8</h4>
                                </div>
                            </div>
                        </div>
                        <form>
                            <div class="form-group">
                                <label>Number of units</label>
                                <input type="text" class="form-control" placeholder="Input the number of Units to be sold">
                            </div>
                            <div class="form-group">
                                <p>
                                    <span> Estimated Amount so far: </span>
                                    <span class="roi-points">N250,000</span>
                                </p>
                                <p>
                                    <span> Estimated Amount on Sales:: </span>
                                    <span class="roi-points">N240,000</span>
                                </p>
                            </div>
                            <div class="form-group">
                                <button type="button" style="display: inherit" class="btn btn-sb-buy mt-5">SELL UNITS</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="trd-invs-wrapper">
            <div class="trd-invs-top">
                <div class="farmshop-header investment mb-0">
                    <div class="farmhead-txt-wrapper">
                        <span>
                            <a href="javascript:void(0)"><img src="{{ asset('/images/angle-invs-right.png') }}"></a>
                        </span>
                    </div>
                    <div>
                        <img src="{{asset('images/asset-131.png')}}" class="farmhead-img" alt="">
                    </div>
                </div>
                <div class="farmshop-dash inv-section-xl">
                    <p class="frm-dash-p mb-0">Dashboard <span class="fa fa-angle-right"></span></p>
                    <p class="frm-dash-p2 mb-0">Investments</p> 
                </div>
                <div class="topinvestments">
                    <div class="ttp-investments mb-xl-0">
                        <p>Total Investments</p>    
                        <h5>N500,000</h5>
                        <div class="lg-visible">
                            <h6>
                                This contains the Sum Total of the Amount Invested <br>
                                with ROI(Return on Investment) as this at this moment
                            </h6>
                        </div>
                        <div class="sellers-list d-none d-xl-block mb-0 mt-xl-0">
                            <a href="/user/dashboard/sellers-list" class="w-100">
                                <img src="{{ asset('/images/sellers_ls.png') }}">
                                <span>see Seller's list</span>
                            </a>
                        </div>
                    </div>
                    <div class="inv-chart">
                        <div class="chart-box">
                            <div id="circle_circle_1" class="circle-progress">
                                <span class="percentage" data-value="25"></span>
                            </div>
                            <h6>Pineapple  Plantation</h6>
                            <div class="chart-timing">
                                <span><img src="{{asset('/images/time_wihite.png')}}" alt=""> 5 Months</span>
                                <span>Units: 4</span>
                            </div>
                            <a href="javascript:void(0)" class="sell_units">sell units</a>
                        </div>
                        <div class="chart-box">
                            <div id="circle_circle_2" class="circle-progress">
                                <span class="percentage" data-value="60"></span>
                            </div>
                            <h6>Habanero Plantation</h6>
                            <div class="chart-timing">
                                <span><img src="{{asset('/images/time_wihite.png')}}" alt=""> 5 Months</span>
                                <span>Units: 4</span>
                            </div>
                            <a href="javascript:void(0)" class="sell_units">sell units</a>
                        </div>
                        <div class="chart-box">
                            <div id="circle_circle_3" class="circle-progress">
                                <span class="percentage" data-value="90"></span>
                            </div>
                            <h6>Cucumber Plantation</h6>
                            <div class="chart-timing">
                                <span><img src="{{asset('/images/time_wihite.png')}}" alt=""> 5 Months</span>
                                <span>Units: 4</span>
                            </div>
                            <a href="javascript:void(0)" class="sell_units">sell units</a>
                        </div
                    </div>
                    <span class="bg-trand"></span>
                </div>
                <div class="sellers-list d-xl-none">
                    <a href="/user/dashboard/sellers-list">
                        <img src="{{ asset('/images/sellers_ls.png') }}">
                        <span>see Seller's list</span>
                    </a>
                </div>
            </div>
            <div class="trd-inv-recents">
                <div class="container-fluid">
                    <div class="row mt-0 section-wrapper">
                        <div class="col-12">
                            <div class="trsct-table">
                                <div id="transc-today" class="table-flex">
                                    <div class="trd-dsh-activities">
                                        <div class="trd-act-wrapper investments-wrap">
                                            <div class="container-fluid px-0">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="trd-frm-435 title-tag-two">
                                                            <div>
                                                                <h6>Transactions</h6>
                                                                <a href="#">
                                                                    See All
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="trd-act627-wrapper">
                                                <ul class="trd-activities-list">
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-2">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Pineapple Plantation</h5>
                                                                <p>25% ROI (6 units at N20,000 per Unit</p>
                                                            </span>
                                                        </div>
                                                        <div class="details type-x">
                                                            <h5 class="price">Active</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-1">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Mango Plantation</h5>
                                                                <p>20% ROI (10 units at N40,000 per Unit</p>
                                                            </span>
                                                        </div>
                                                        <div class="details">
                                                            <h5 class="price">Paid</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-2">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Pawpaw Plantation</h5>
                                                                <p>25% ROI (10 units at N30,000 per Unit</p>
                                                            </span>
                                                        </div>
                                                        <div class="details">
                                                            <h5 class="price">Paid</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                    <li class="trd-activities-item">
                                                        <div class="items">
                                                            <span class="item-img img-type-1">
                                                                <img src="{{ asset('/images/savings.svg') }}" alt="">
                                                            </span>
                                                            <span class="item-tag">
                                                                <h5>Habanero Plantation</h5>
                                                                <p>25% ROI (6 units at N20,000 per Unit</p>
                                                            </span>
                                                        </div>
                                                        <div class="details type-x">
                                                            <h5 class="price">Active</h5>
                                                            <p class="date">Aug. 5, 2020</p>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>                        
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </main>
@endsection

@section('script')
    <script src="{{ asset('/js/circle-progress.js') }}"></script>
    <script>
        var c1 = $('#circle_circle_1');
        var c2 = $('#circle_circle_2');
        var c3 = $('#circle_circle_3');
        var c4 = $('#plantation_chart');

        function drawChart(box, empty = true){
            var chartVal = box.find(".percentage").attr("data-value");
            box.children().text(`${chartVal}%`)

            if($(document).width() >= 1200){
                box.circleProgress({
                    startAngle: -Math.PI / 4 * 2,
                    value: chartVal / 100,
                    size: 140,
                    thickness: 22,
                    emptyFill: "#ffbe74",
                    lineCap: 'round',
                    fill: {color: '#06fd97'}
                });
            }else{
                box.circleProgress({
                    startAngle: -Math.PI / 4 * 2,
                    value: chartVal / 100,
                    size: 100,
                    thickness: 17,
                    emptyFill: "#ffbe74",
                    lineCap: 'round',
                    fill: {color: '#06fd97'}
                });
            }
        }        

        drawChart(c1)
        drawChart(c2)
        drawChart(c3)
        drawChart(c4)
        
    </script>
    <script>
        document.querySelectorAll('.sell_units').forEach(unit => {
            unit.addEventListener('click', function(){
                addDom('#plantation_sales', 'show')
            })
        });
    </script>
@endsection
