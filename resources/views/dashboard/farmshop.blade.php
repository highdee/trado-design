@extends('layouts.dashboard')

@section('content')
    <main>
        {{-- farmshop header --}}
        <div class="farmshop-header">
            <div class="farmhead-txt-wrapper">
                <h4 class="farm-h4">FarmShop</h4>
                <p class="farm-p">Check Out Available plantations</p>
            </div>
            <div >
                <img src="{{asset('images/asset-131.png')}}" class="farmhead-img" alt="">
            </div>
        </div>
        <div class="farmshop-dash">
            <p class="frm-dash-p">Dashboard <span class="fa fa-angle-right"></span></p>
            <p class="frm-dash-p2">FarmShop</p>
        </div>
        <div class="row mt-0 farmshop-wrapper">
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 fs-c">
                <div class="farmshop-card">
                    <div class="farm-sec1">
                        <img class="farmshop-img" src="{{ asset('/images/asset-91.png') }}" alt="">
                        <div class="farm-details">
                            <p class="plantation">Name of Plantation </p>
                            <h5 class="famrshop-plant-name ">Cucumber</h5>
                            <div class="plant-cost p-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/money_1.png')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cost per unit</p>
                                    <p class="f-amt">N20,000</p>
                                </div>
                            </div>
                            <div class="plant-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/time_1.svg')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cycle duration</p>
                                    <p class="duration">6 Months</p>
                                </div>
                            </div>
                            <span class="roi">ROI:25%</span>
                        </div>
                    </div>
                    <div class="farm-sec2">
                        <div class="farm-location">
                            <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                            <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                        </div>
                        <a href="javascript:void(0)" class="farm-btn btn btn-success btn-lg">INVEST NOW</a>
                        <div class="farmshop-countdown">
                            <p class="farmshop-cd">Countdown <span class="frm-cd-num">02</span> DAYS , <span class="frm-cd-num">10</span> HRS. <span class="frm-cd-num">02</span> MINS</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 fs-c">
                <div class="farmshop-card">
                    <div class="farm-sec1">
                        <img class="farmshop-img" src="{{asset('images/Habanero-8.png')}}" alt="">
                        <div class="farm-details">
                            <p class="plantation">Name of Plantation </p>
                            <h5 class="famrshop-plant-name">Cucumber</h5>
                            <div class="plant-cost p-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/money_1.png')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cost per unit</p>
                                    <p class="f-amt">N20,000</p>
                                </div>
                            </div>
                            <div class="plant-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/time_1.svg')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cycle duration</p>
                                    <p class="duration">6 Months</p>
                                </div>
                            </div>
                            <span class="roi">ROI:25%</span>
                        </div>
                    </div>
                    <div class="farm-sec2">
                        <div class="farm-location">
                            <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                            <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                        </div>
                        <a href="javascript:void(0)" class="farm-btn btn btn-success btn-lg sold-out">Sold Out</a>
                        <div class="farmshop-countdown">
                            <p class="farmshop-cd">Countdown <span class="frm-cd-num">02</span> DAYS , <span class="frm-cd-num">10</span> HRS. <span class="frm-cd-num">02</span> MINS</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 fs-c">
                <div class="farmshop-card">
                    <div class="farm-sec1">
                        <img class="farmshop-img" src="{{asset('images/miguel-andrade-nAOZCYcLND-7.png')}}" alt="">
                        <div class="farm-details">
                            <p class="plantation">Name of Plantation </p>
                            <h5 class="famrshop-plant-name">Cucumber</h5>
                            <div class="plant-cost p-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/time_1.svg')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cost per unit</p>
                                    <p class="f-amt">N20,000</p>
                                </div>
                            </div>
                            <div class="plant-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/money_1.png')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cycle duration</p>
                                    <p class="duration">6 Months</p>
                                </div>
                            </div>
                            <span class="roi">ROI:25%</span>
                        </div>
                    </div>
                    <div class="farm-sec2">
                        <div class="farm-location">
                            <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                            <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                        </div>
                        <a href="javascript:void(0)" class="farm-btn btn btn-success btn-lg">INVEST NOW</a><div class="farmshop-countdown">
                            <p class="farmshop-cd">Countdown <span class="frm-cd-num">02</span> DAYS , <span class="frm-cd-num">10</span> HRS. <span class="frm-cd-num">02</span> MINS</p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 fs-c">
                <div class="farmshop-card">
                    <div class="farm-sec1">
                        <img class="farmshop-img" src="{{asset('images/asset-91.png')}}" alt="">
                        <div class="farm-details">
                            <p class="plantation">Name of Plantation </p>
                            <h5 class="famrshop-plant-name ">Cucumber</h5>
                            <div class="plant-cost p-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/money_1.png')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cost per unit</p>
                                    <p class="f-amt">N20,000</p>
                                </div>
                            </div>
                            <div class="plant-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/time_1.svg')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cycle duration</p>
                                    <p class="duration">6 Months</p>
                                </div>
                            </div>
                            <span class="roi">ROI:25%</span>
                        </div>
                    </div>
                    <div class="farm-sec2">
                        <div class="farm-location">
                            <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                            <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                        </div>
                        <a href="javascript:void(0)" class="farm-btn btn btn-success btn-lg">INVEST NOW</a>
                        <div class="farmshop-countdown">
                            <p class="farmshop-cd">Countdown <span class="frm-cd-num">02</span> DAYS , <span class="frm-cd-num">10</span> HRS. <span class="frm-cd-num">02</span> MINS</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 fs-c">
                <div class="farmshop-card">
                    <div class="farm-sec1">
                        <img class="farmshop-img" src="{{asset('images/Habanero-8.png')}}" alt="">
                        <div class="farm-details">
                            <p class="plantation">Name of Plantation </p>
                            <h5 class="famrshop-plant-name">Cucumber</h5>
                            <div class="plant-cost p-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/money_1.png')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cost per unit</p>
                                    <p class="f-amt">N20,000</p>
                                </div>
                            </div>
                            <div class="plant-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/time_1.svg')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cycle duration</p>
                                    <p class="duration">6 Months</p>
                                </div>
                            </div>
                            <span class="roi">ROI:25%</span>
                        </div>
                    </div>
                    <div class="farm-sec2">
                        <div class="farm-location">
                            <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                            <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                        </div>
                        <a href="javascript:void(0)" class="farm-btn btn btn-success btn-lg sold-out">Sold Out</a>
                        <div class="farmshop-countdown">
                            <p class="farmshop-cd">Countdown <span class="frm-cd-num">02</span> DAYS , <span class="frm-cd-num">10</span> HRS. <span class="frm-cd-num">02</span> MINS</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 fs-c">
                <div class="farmshop-card">
                    <div class="farm-sec1">
                        <img class="farmshop-img" src="{{asset('images/miguel-andrade-nAOZCYcLND-7.png')}}" alt="">
                        <div class="farm-details">
                            <p class="plantation">Name of Plantation </p>
                            <h5 class="famrshop-plant-name">Cucumber</h5>
                            <div class="plant-cost p-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/time_1.svg')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cost per unit</p>
                                    <p class="f-amt">N20,000</p>
                                </div>
                            </div>
                            <div class="plant-cost">
                                <div class="farm-i">
                                    <img src="{{asset('images/money_1.png')}}" alt="">
                                </div>
                                <div class="plant-amt">
                                    <p class="cycle">cycle duration</p>
                                    <p class="duration">6 Months</p>
                                </div>
                            </div>
                            <span class="roi">ROI:25%</span>
                        </div>
                    </div>
                    <div class="farm-sec2">
                        <div class="farm-location">
                            <div class="farm-i locate-i"><i class="fa fa-clock-o"></i></div>
                            <span class="f-locate">Ajaokuta farm, Iwo, Osun State </span>
                        </div>
                        <a href="javascript:void(0)" class="farm-btn btn btn-success btn-lg">INVEST NOW</a><div class="farmshop-countdown">
                            <p class="farmshop-cd">Countdown <span class="frm-cd-num">02</span> DAYS , <span class="frm-cd-num">10</span> HRS. <span class="frm-cd-num">02</span> MINS</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
