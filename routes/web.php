<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('pages.index');
});

Route::get('/sign-up', function () {
    return view('pages/sign_up');
});

Route::get('/login', function () {
    return view('pages/login');
});

Route::get('/forgot-password', function () {
    return view('pages/forgotpassword');
});

Route::get('/faqs', function () {
    return view('pages/faqs');
});

Route::get('/blog', function () {
    return view('pages/blog');
});
Route::get('/about-us', function () {
    return view('pages/aboutUs');
});
Route::get('/contact-us', function () {
    return view('pages/contact-us');
});

//DASHBBOARD
Route::get('/user/dashboard', function () {
    return view('dashboard/index');
});
Route::get('/user/dashboard/farmshop', function () {
    return view('dashboard/farmshop');
});
Route::get('/user/dashboard/transaction', function () {
    return view('dashboard/transaction');
});
Route::get('/user/dashboard/transaction/investments', function () {
    return view('dashboard/investments');
});
Route::get('/user/dashboard/transaction/investment_unit', function () {
    return view('dashboard/investment_list_unit');
});
Route::get('/user/dashboard/settings', function () {
    return view('dashboard/settings/settings');
});
Route::get('/user/dashboard/project-update', function () {
    return view('dashboard/project-update');
});
Route::get('/user/dashboard/sellers-list', function () {
    return view('dashboard/sellers-list');
});
Route::get('/user/dashboard/savings', function () {
    return view('dashboard/savings');
});

//error
Route::get('/page-not-found', function () {
    return view('errorPages.404');
});
