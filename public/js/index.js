$(document).ready( function() {
    $('.navbar-toggler').click(function() {
        $('.nav-col').toggleClass('nav-fixed');
    });


    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        navText:["<span class='fa fa-angle-left caro-nav prev-slide'></span>","<span class='fa fa-angle-right caro-nav next-slide'></span>"],
        responsive:{
            0:{
                items:1
            },
            770:{
                items:2
            },
            997:{
                items:3
            },
            1000:{
                items:3
            },
            1200:{
                items:4
            },

        }
    })
})
